package net.protolauncher.api;

import com.google.gson.Gson;
import com.google.gson.annotations.Since;
import net.protolauncher.api.modding.FabricVersionInfo;
import net.protolauncher.api.modding.ForgeVersionInfo;
import net.protolauncher.mojang.version.VersionInfo;
import net.protolauncher.mojang.version.VersionType;
import net.protolauncher.util.IStorable;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.UUID;

public class Profile implements IStorable {

    // Constants
    public static final double STORE_VERSION;
    public static final Path STORE_LOCATION;
    public static final Gson GSON;

    // Static Initializer
    static {
        STORE_VERSION = 1.00;
        STORE_LOCATION = Path.of("launcher/profiles.json");
        GSON = ProtoLauncher.GSON.newBuilder().setVersion(STORE_VERSION).create();
    }

    // Properties
    @Since(1.00)
    private String name;

    @Since(1.00)
    private String uuid;

    @Since(1.00)
    private String owner;

    @Since(1.00)
    private String path;

    @Since(1.00)
    private Version version;

    @Since(1.00)
    private LaunchSettings launchSettings;

    @Since(1.00)
    private ProfileSettings profileSettings;

    /**
     * Copies the properties of the {@link ProfileBuilder} to the {@link Profile}.
     * @param builder The {@link ProfileBuilder}
     */
    public Profile(ProfileBuilder builder) {
        this.name = builder.name;
        this.uuid = builder.uuid;
        this.owner = builder.owner;
        this.path = builder.path;
        this.version = builder.version;
        this.launchSettings = builder.launchSettings;
        this.profileSettings = builder.profileSettings;
    }

    /**
     * Constructs a new profile builder with the same properties as this profile.
     * @return A new {@link ProfileBuilder}
     */
    public ProfileBuilder newBuilder() {
        return new ProfileBuilder(this);
    }

    /**
     * Fetches the default preview icon from the JAR for this profile.
     * @return The image resource as a stream.
     */
    public InputStream getDefaultPreviewIcon() {
        // Modded icon
        if (this.version.forge != null || this.version.fabric != null) {
            return getClass().getResourceAsStream("/images/profile_modded.png");
        }

        // Snapshot icon
        if (this.version.type == VersionType.SNAPSHOT) {
            return getClass().getResourceAsStream("/images/profile_snapshot.png");
        }

        // Vanilla icon
        return getClass().getResourceAsStream("/images/profile_vanilla.png");
    }

    /**
     * Creates the default location for this profile.
     *
     * @param name The name of this profile
     * @param owner The UUID of the owner of this profile
     * @return The {@link Path} to the location of this profile
     * @throws IOException Thrown if any filesystem access goes wrong
     */
    public static Path getDefaultLocation(String name, String owner) throws IOException {
        Path loc = Path.of("profiles/" + owner + "/" + name + "/");
        int count = 0;
        while (Files.exists(loc, LinkOption.NOFOLLOW_LINKS)) {
            count++;
            loc = Path.of("profiles/" + owner + "/" + name + "-" + count + "/");
        }
        Files.createDirectories(loc);
        return loc;
    }

    // Getters
    public String getName() {
        return name;
    }
    public String getUuid() {
        return uuid;
    }
    public String getOwnerUuid() {
        return owner;
    }
    public String getPath() {
        return path;
    }
    public Version getVersion() {
        return version;
    }
    public LaunchSettings getLaunchSettings() {
        return launchSettings;
    }
    public ProfileSettings getProfileSettings() {
        return profileSettings;
    }

    // Stringify
    @Override
    public String toString() {
        return "Profile{" +
                "name='" + name + '\'' +
                ", uuid='" + uuid + '\'' +
                ", owner='" + owner + '\'' +
                ", path='" + path + '\'' +
                ", version=" + version +
                ", launchSettings=" + launchSettings +
                ", profileSettings=" + profileSettings +
                '}';
    }

    // Sub-Objects
    public static class ProfileBuilder {

        // Properties
        private String name;
        private String uuid;
        private String owner;
        private String path;
        private Version version;
        private LaunchSettings launchSettings;
        private ProfileSettings profileSettings;

        /**
         * Constructs a new ProfileBuilder.
         *
         * @param name The name of this profile
         * @param mcv The mcv for this profile
         * @param owner The owner who owns this profile
         * @throws IOException Thrown if something goes wrong preparing the filesystem
         */
        public ProfileBuilder(String name, VersionInfo mcv, User owner) throws IOException {
            this.name = name;
            this.uuid = UUID.randomUUID().toString().replace("-", "");
            this.owner = owner.getUuid();
            this.path = getDefaultLocation(name, owner.getUuid()).toAbsolutePath().toString();
            this.version = new Version();
            this.version.type = mcv.getType();
            this.version.minecraft = mcv.getId();
            this.launchSettings = new LaunchSettings();
            this.launchSettings.gameResolutionX = -1;
            this.launchSettings.gameResolutionY = -1;
            this.profileSettings = new ProfileSettings();
        }

        /**
         * Constructs a new ProfileBuilder from an existing profile.
         *
         * @param profile The existing profile.
         */
        public ProfileBuilder(Profile profile) {
            this.name = profile.name;
            this.uuid = profile.uuid;
            this.owner = profile.owner;
            this.path = profile.path;
            this.version = profile.version;
            this.launchSettings = profile.launchSettings;
            this.profileSettings = profile.profileSettings;
        }

        /**
         * Sets a new name for this profile.
         */
        public ProfileBuilder setName(String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets this profile to use a different owner.
         */
        public ProfileBuilder setOwner(User owner) {
            this.owner = owner.getUuid();
            return this;
        }

        /**
         * Changes the path for this profile.
         */
        public ProfileBuilder setPath(Path path) {
            this.path = path.toAbsolutePath().toString();
            return this;
        }

        /**
         * Sets this profile to use a new Minecraft version.
         */
        public ProfileBuilder setMinecraftVersion(VersionInfo mcv) {
            this.version.type = mcv.getType();
            this.version.minecraft = mcv.getId();
            return this;
        }

        /**
         * Sets this profile to use a new Minecraft Forge version.<br/>
         * This also ensures the profile launches with Forge.<br/>
         * Will remove a Fabric version if there is one.
         */
        public ProfileBuilder setForgeVersion(@Nullable ForgeVersionInfo fvi) {
            if (this.version.fabric != null) {
                this.version.fabric = null;
            }
            this.version.forge = fvi != null ? fvi.getId() : null;
            return this;
        }

        /**
         * Sets this profile to use a new Minecraft Fabric version.<br/>
         * This also ensures the profile launches with Fabric.<br/>
         * Will remove a Forge version if there is one.
         */
        public ProfileBuilder setFabricVersion(@Nullable FabricVersionInfo fvi) {
            if (this.version.forge != null) {
                this.version.forge = null;
            }
            this.version.fabric = fvi != null ? fvi.getId() : null;
            return this;
        }

        /**
         * Sets whether or not the version should keep itself updated to the latest update.
         */
        public ProfileBuilder setLatest(boolean latest) {
            this.version.latest = latest;
            return this;
        }

        /**
         * Sets the width and height launch settings for this profile.
         */
        public ProfileBuilder setGameResolution(int x, int y) {
            this.launchSettings.gameResolutionX = x;
            this.launchSettings.gameResolutionY = y;
            return this;
        }

        /**
         * Sets the javaPath in the launch settings to the java executable for this profile.
         */
        public ProfileBuilder setJavaPath(@Nullable Path path) {
            this.launchSettings.javaPath = path != null ? path.toAbsolutePath().toString() : null;
            return this;
        }

        /**
         * Sets the JVM Launch Arguments for this profile.
         */
        public ProfileBuilder setJvmArguments(@Nullable String args) {
            this.launchSettings.jvmArguments = args;
            return this;
        }

        /**
         * Sets the preview icon path for this profile.
         */
        public ProfileBuilder setPreviewIconPath(@Nullable Path path) {
            this.profileSettings.previewIconPath = path != null ? path.toAbsolutePath().toString() : null;
            return this;
        }

        /**
         * Sets the number that determines the order of this profile in the profiles list for the profile's owner's user.
         */
        public ProfileBuilder setOrder(int order) {
            this.profileSettings.order = order;
            return this;
        }

        // Create
        public Profile create() {
            return new Profile(this);
        }

    }
    public static class Version {

        // Properties
        @Since(1.00)
        private VersionType type;

        @Since(1.00)
        private String minecraft;

        @Since(1.00)
        @Nullable
        private String forge;

        @Since(1.00)
        @Nullable
        private String fabric;

        @Since(1.00)
        private boolean latest;

        /**
         * Constructs a new profile version.
         */
        private Version() { }

        // Getters
        public VersionType getType() {
            return type;
        }
        public String getMinecraft() {
            return minecraft;
        }
        public @Nullable String getForge() {
            return forge;
        }
        public @Nullable String getFabric() {
            return fabric;
        }
        public boolean isLatest() {
            return latest;
        }

    }
    public static class LaunchSettings {

        // Properties
        @Since(1.00)
        private int gameResolutionX;

        @Since(1.00)
        private int gameResolutionY;

        @Since(1.00)
        @Nullable
        private String javaPath;

        @Since(1.00)
        @Nullable
        private String jvmArguments;

        /**
         * Constructs a new profile launch settings.
         */
        private LaunchSettings() { }

        // Getters
        public int getGameResolutionX() {
            return gameResolutionX;
        }
        public int getGameResolutionY() {
            return gameResolutionY;
        }
        public @Nullable String getJavaPath() {
            return javaPath;
        }
        public @Nullable String getJvmArguments() {
            return jvmArguments;
        }
    }
    public static class ProfileSettings {

        // Properties
        @Since(1.00)
        private @Nullable String previewIconPath;

        @Since(1.00)
        private int order;

        /**
         * Constructs a new profile settings.
         */
        private ProfileSettings() { }

        // Getters
        public @Nullable String getPreviewIconPath() {
            return previewIconPath;
        }
        public int getOrder() {
            return order;
        }

    }

}
