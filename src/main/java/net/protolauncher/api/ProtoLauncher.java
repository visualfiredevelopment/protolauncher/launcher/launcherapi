package net.protolauncher.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import javafx.stage.Stage;
import javafx.util.Pair;
import net.protolauncher.api.Profile.ProfileBuilder;
import net.protolauncher.api.Settings.Endpoints;
import net.protolauncher.api.modding.FabricVersionInfo;
import net.protolauncher.api.modding.FabricVersionManifest;
import net.protolauncher.api.modding.ForgeVersionInfo;
import net.protolauncher.api.modding.ForgeVersionManifest;
import net.protolauncher.api.task.LauncherTask.DummyTaskFeedback;
import net.protolauncher.api.task.LauncherTask.TaskAnonymousFeedback;
import net.protolauncher.api.task.LauncherTask.TaskFeedback;
import net.protolauncher.mojang.Artifact;
import net.protolauncher.mojang.asset.Asset;
import net.protolauncher.mojang.asset.AssetIndex;
import net.protolauncher.mojang.auth.MicrosoftAuth;
import net.protolauncher.mojang.auth.Yggdrasil;
import net.protolauncher.mojang.library.Library;
import net.protolauncher.mojang.rule.Action;
import net.protolauncher.mojang.rule.Rule;
import net.protolauncher.mojang.version.Version;
import net.protolauncher.mojang.version.VersionInfo;
import net.protolauncher.mojang.version.VersionManifest;
import net.protolauncher.mojang.version.VersionType;
import net.protolauncher.util.Network;
import net.protolauncher.util.Store;
import net.protolauncher.util.SystemInfo;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.*;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

public final class ProtoLauncher {

    // Constants
    public static final Gson GSON = new GsonBuilder().serializeNulls().create();
    public static final Logger LOGGER = LogManager.getLogger();
    public static final Path FORGE_WRAPPER = Path.of("launcher/lib/ForgeWrapper-1.4.2.jar");

    // Singleton
    private static final ProtoLauncher instance = new ProtoLauncher();

    // Variables
    private boolean initialized;
    private Settings settings;
    private Store<User> users;
    private Store<Profile> profiles;
    private VersionManifest versionManifest;
    private ForgeVersionManifest forgeVersionManifest;
    private FabricVersionManifest fabricVersionManifest;
    private Yggdrasil yggdrasil;
    private MicrosoftAuth microsoftAuth;

    // Constructor
    private ProtoLauncher() {
        this.initialized = false;
    }

    // Methods
    /**
     * Initializes the launcher. Performs the following tasks:<br/>
     * - Loads the Settings<br/>
     * - Loads the Users<br/>
     * - Loads the Profiles<br/>
     * - Prepares the Authentication Objects<br/>
     * - Fetches the Version Manifest<br/>
     *
     * @param feedback The {@link TaskFeedback} for this method.
     * @throws IOException Thrown if something goes wrong.
     */
    public void initialize(TaskFeedback<Void> feedback) throws IOException {
        if (initialized) {
            return;
        }
        initialized = true;

        long stepsCompleted = 0;
        long maxSteps = 6;

        // Load Settings
        feedback.updateMessage("Loading settings...");
        settings = new Settings();
        if (!Files.exists(settings.getLocation(), LinkOption.NOFOLLOW_LINKS)) {
            settings.reset(null, null, null).save();
        }
        settings.load();
        feedback.updateMessage("Settings loaded.");
        feedback.updateProgress(++stepsCompleted, maxSteps);

        // Load Users
        feedback.updateMessage("Loading users...");
        users = new Store<>(User.GSON, User.STORE_LOCATION, User.class);
        users.load();
        feedback.updateMessage("Users loaded. Found " + users.size() + " users.");
        feedback.updateProgress(++stepsCompleted, maxSteps);

        // Load Profiles
        feedback.updateMessage("Loading profiles...");
        profiles = new Store<>(Profile.GSON, Profile.STORE_LOCATION, Profile.class);
        profiles.load();
        feedback.updateMessage("Profiles loaded. Found " + profiles.size() + " profiles.");
        feedback.updateProgress(++stepsCompleted, maxSteps);

        // Create Authentication Objects
        feedback.updateMessage("Preparing authentication...");
        Endpoints endpoints = settings.getEndpoints();
        yggdrasil = new Yggdrasil(User.GSON, endpoints.getYggdrasilApi().toString(), settings.getClientToken());
        microsoftAuth = new MicrosoftAuth(
            User.GSON,
            endpoints.getMicrosoftApi().getClientId(),
            endpoints.getMicrosoftApi().getRedirectUrl(),
            endpoints.getMicrosoftApi().getOauthAuthorizeUrl().toString(),
            endpoints.getMicrosoftApi().getOauthTokenUrl().toString(),
            endpoints.getMicrosoftApi().getXblUrl().toString(),
            endpoints.getMicrosoftApi().getXstsUrl().toString(),
            endpoints.getMicrosoftApi().getMcsUrl().toString()
        );
        feedback.updateMessage("Authentication ready.");
        feedback.updateProgress(++stepsCompleted, maxSteps);

        // Check Version Manifest
        feedback.updateMessage("Fetching version manifests...");
        Pair<VersionManifest, Boolean> vmp = VersionManifest.load(endpoints.getManifest(), settings.getLastManifestUpdate().plus(settings.getMaxManifestAge()));
        Pair<ForgeVersionManifest, Boolean> fvmp = ForgeVersionManifest.load(endpoints.getForgeManifest(), settings.getLastManifestUpdate().plus(settings.getMaxManifestAge()));
        Pair<FabricVersionManifest, Boolean> fbvmp = FabricVersionManifest.load(endpoints.getFabricManifest(), settings.getLastManifestUpdate().plus(settings.getMaxManifestAge()));
        versionManifest = vmp.getKey();
        forgeVersionManifest = fvmp.getKey();
        fabricVersionManifest = fbvmp.getKey();
        if (vmp.getValue() || fvmp.getValue() || fbvmp.getValue()) {
            settings.setLastManifestUpdate(Instant.now()).save();
            feedback.updateMessage("Manifests updated.");
        } else {
            feedback.updateMessage("Manifests are up to date.");
        }
        feedback.updateProgress(++stepsCompleted, maxSteps);

        // Check Latest Profiles
        this.checkLatestProfiles(feedback);
        feedback.updateProgress(++stepsCompleted, maxSteps);
    }
    public void initialize() throws IOException {
        initialize(new DummyTaskFeedback<>());
    }

    /**
     * Adds a user to the launcher via Microsoft login.
     * @param feedback The {@link TaskFeedback} for this method.
     * @param owner The optional stage owner for the login popup.
     * @throws Exception Thrown if something goes wrong.
     */
    public User addUserMicrosoft(TaskFeedback<User> feedback, @Nullable Stage owner) throws Exception {
        // Prompt
        feedback.updateMessage("Prompting for login...");
        String code = microsoftAuth.promptMicrosoftLogin(owner).get();
        if (code == null) {
            feedback.updateMessage("Login cancelled.");
            return null;
        }

        // Authenticate
        feedback.updateMessage("Authenticating...");
        User user = User.loginMicrosoft(code);

        // Add User
        this.addUser(feedback, user, true);

        // Return new user
        return user;
    }
    public User addUserMicrosoft(@Nullable Stage owner) throws Exception {
        return addUserMicrosoft(new DummyTaskFeedback<>(), owner);
    }

    /**
     * Adds a user to the launcher via Mojang login.
     * @param feedback The {@link TaskFeedback} for this method.
     * @param username The username to login with.
     * @param password The password to login with.
     * @throws Exception Thrown if something goes wrong.
     */
    public User addUserMojang(TaskFeedback<User> feedback, String username, String password) throws Exception {
        // Authenticate
        feedback.updateMessage("Authenticating...");
        User user = User.loginMojang(username, password);

        // Add User
        this.addUser(feedback, user, true);

        // Return user
        return user;
    }
    public User addUserMojang(String username, String password) throws Exception {
        return addUserMojang(new DummyTaskFeedback<>(), username, password);
    }

    /**
     * Adds a user to the launcher, overriding an existing user if there is one.
     * @param feedback The {@link TaskFeedback} for this method.
     * @param user The user to add to the launcher.
     * @param makeDefaultProfiles Whether or not to create the default profiles for this user.
     * @throws IOException Thrown if something goes wrong.
     */
    public void addUser(TaskAnonymousFeedback feedback, User user, boolean makeDefaultProfiles) throws IOException {
        // Add User & Set Current User
        users.put(user).save();

        // Make Default Profiles
        if (makeDefaultProfiles) {
            feedback.updateMessage("Making default profiles...");
            this.makeLatestReleaseProfile(user);
            this.makeLatestSnapshotProfile(user);
            this.makeLatestForgeProfile(user);
            feedback.updateMessage("Default profiles created.");
        }

        // Switch User
        feedback.updateMessage("Switching users...");
        this.switchUser(user);
        feedback.updateMessage("User switched.");

        // Finished!
        feedback.updateMessage("Logged in!");
    }

    /**
     * Switches the launcher from one user to another.
     * @param user The user to switch to.
     * @throws IOException Thrown if something goes wrong.
     */
    public void switchUser(@Nullable User user) throws IOException {
        // Set Current User
        settings.setCurrentUser(user).save();

        // Check Current User
        if (user != null) {
            // Find First Profile
            Profile profile = profiles.values().stream().parallel()
                    .filter(p -> p.getOwnerUuid().equals(user.getUuid()))
                    .findFirst().orElse(null);

            // Switch to Profile
            this.switchProfile(profile);

            // Check Latest Profiles
            this.checkLatestProfiles();
        } else {
            // Switch to null profile
            this.switchProfile(null);
        }
    }

    /**
     * Removes a user from the launcher and updates the current user.
     *
     * @param user The user to remove.
     * @throws IOException Thrown if something goes wrong.
     */
    public void removeUser(User user) throws IOException {
        // Do we need to invalidate the accessToken? Or is throwing it away fine?
        users.remove(user).save();

        assert settings.getCurrentUserUuid() != null; // Due to settings.hasCurrentUser() check
        if (settings.hasCurrentUser() && settings.getCurrentUserUuid().equals(user.getUuid())) {
            if (users.size() > 0) {
                settings.setCurrentUser(users.values().toArray(User[]::new)[0]);
            } else {
                settings.setCurrentUser(null);
            }
            settings.save();
        }
    }

    /**
     * Adds a profile to the launcher and then switches to it.
     *
     * @param profile The profile to add.
     * @throws IOException Thrown if something goes wrong.
     */
    public void addProfile(Profile profile) throws IOException {
        // Add profile and save
        profiles.put(profile).save();

        // Switch profiles
        this.switchProfile(profile);
    }

    /**
     * Switches the launcher from one profile to another.
     * @param profile The profile to switch to.
     * @throws IOException Thrown if something goes wrong.
     */
    public void switchProfile(@Nullable Profile profile) throws IOException {
        settings.setCurrentProfile(profile).save();

        // Check for different user and if so switch
        if (profile != null && !profile.getOwnerUuid().equals(settings.getCurrentUserUuid())) {
            this.switchUser(users.get(profile.getOwnerUuid()));
        }
    }

    /**
     * Removes a profile from the launcher.
     *
     * @param profile The profile to remove.
     * @throws IOException Thrown if something goes wrong.
     */
    public void removeProfile(Profile profile) throws IOException {
        profiles.remove(profile).save();
    }

    /**
     * Fetches an array of profiles for the specific user.
     * @param user The user who owns these profiles.
     * @return A {@link Profile} array.
     */
    public Profile[] getProfilesForUser(User user, boolean inOrder) {
        if (inOrder) {
            return profiles.values().stream().parallel().filter(p -> p.getOwnerUuid().equals(user.getUuid())).sorted(Comparator.comparingInt(p -> p.getProfileSettings().getOrder())).toArray(Profile[]::new);
        } else {
            return profiles.values().stream().parallel().filter(p -> p.getOwnerUuid().equals(user.getUuid())).toArray(Profile[]::new);
        }
    }
    public Profile[] getProfilesForUser(User user) {
        return this.getProfilesForUser(user, false);
    }

    /**
     * Moves the order of a specific profile up one.
     * @param profile The profile to move up.
     */
    public void moveProfileDown(Profile profile) throws Exception {
        // Get user profiles
        Profile[] userProfiles = this.getProfilesForUser(this.getUsers().get(profile.getOwnerUuid()));

        // Get new pos
        int newPos = profile.getProfileSettings().getOrder() + 1;
        if (newPos > userProfiles.length) {
            return;
        }

        // Get next profile
        for (Profile userProfile : userProfiles) {
            if (userProfile.getProfileSettings().getOrder() == newPos) {
                // Set next profile to our position
                profiles.put(userProfile.newBuilder().setOrder(profile.getProfileSettings().getOrder()).create());
                break;
            }
        }

        // Set our profile to their position
        profiles.put(profile.newBuilder().setOrder(newPos).create());

        // Save profiles
        profiles.save();
    }

    /**
     * Moves the order of a specific profile down one.
     * @param profile The profile to move down.
     */
    public void moveProfileUp(Profile profile) throws Exception {
        // Get user profiles
        Profile[] userProfiles = this.getProfilesForUser(this.getUsers().get(profile.getOwnerUuid()));

        // Get new pos
        int newPos = profile.getProfileSettings().getOrder() - 1;
        if (newPos <= 0) {
            return;
        }

        // Get prev profile
        for (Profile userProfile : userProfiles) {
            if (userProfile.getProfileSettings().getOrder() == newPos) {
                // Set next profile to our position
                profiles.put(userProfile.newBuilder().setOrder(profile.getProfileSettings().getOrder()).create());
                break;
            }
        }

        // Update profile
        profiles.put(profile.newBuilder().setOrder(newPos).create());

        // Save profiles
        profiles.save();
    }

    /**
     * Checks for the 'LATEST' profiles and updates their MCV if it is outdated.
     *
     * @param feedback The {@link TaskFeedback} for this method.
     * @throws IOException Thrown if something goes wrong.
     */
    public void checkLatestProfiles(TaskFeedback<Void> feedback) throws IOException {
        feedback.updateMessage("Checking latest profiles...");

        // Get User
        User currentUser = settings.getCurrentUser(users);
        if (currentUser == null) {
            feedback.updateMessage("No current user? Huh, okay.");
            return;
        }

        // Get latest profiles
        Profile[] latestProfiles = profiles.values().stream().parallel()
                .filter(p ->
                    p.getOwnerUuid().equals(currentUser.getUuid()) &&
                    p.getVersion().isLatest())
                .toArray(Profile[]::new);

        // Loop
        for (Profile profile : latestProfiles) {
            Profile.Version ver = profile.getVersion();
            boolean updated = false;

            // TODO: If a profile has Fabric/Forge, this does not update that. We need to add a check for that!
            if (ver.getFabric() != null || ver.getForge() != null) {
                continue;
            }

            // Check each version and fetch the latest correspondingly
            // Then check if the version is outdated, if it is, update the profile
            if (ver.getType() == VersionType.RELEASE) {
                VersionInfo lrvi = versionManifest.getLatestRelease();
                if (!ver.getMinecraft().equals(lrvi.getId())) {
                    profile = profile.newBuilder().setMinecraftVersion(lrvi).create();
                    feedback.updateMessage("Updated the " + profile.getName() + " profile to Minecraft " + lrvi.getId() + ".");
                    updated = true;
                }
            } else if (ver.getType() == VersionType.SNAPSHOT) {
                VersionInfo lsvi = versionManifest.getLatestSnapshot();
                if (!ver.getMinecraft().equals(lsvi.getId())) {
                    profile = profile.newBuilder().setMinecraftVersion(lsvi).create();
                    feedback.updateMessage("Updated the " + profile.getName() + " profile to Minecraft " + lsvi.getId() + ".");
                    updated = true;
                }
            }

            // Update and save
            if (updated) {
                profiles.put(profile).save();
            }
        }

        // Done
        feedback.updateMessage("All profiles up to date.");
    }
    public void checkLatestProfiles() throws IOException {
        checkLatestProfiles(new DummyTaskFeedback<>());
    }

    /**
     * Creates and saves the 'Latest Release' profile for the specified user.
     * @param owner The owner of this profile.
     * @throws IOException Thrown if something goes wrong.
     */
    public void makeLatestReleaseProfile(User owner) throws IOException {
        // Attempt to find existing profile
        Profile existing = profiles.values().stream().parallel()
                .filter(p ->
                    p.getOwnerUuid().equals(owner.getUuid()) &&
                    p.getVersion().getType() == VersionType.RELEASE &&
                    p.getVersion().isLatest())
                .findFirst().orElse(null);
        if (existing != null) {
            return;
        }

        // Create Profile
        Profile profile = new ProfileBuilder(
            "Latest Release",
            versionManifest.getLatestRelease(),
            owner
        ).setLatest(true).create();
        profiles.put(profile).save();
    }

    /**
     * Creates and saves the 'Latest Snapshot' profile for the specified user.
     * @param owner The owner of this profile.
     * @throws IOException Thrown if something goes wrong.
     */
    public void makeLatestSnapshotProfile(User owner) throws IOException {
        // Attempt to find existing profile
        Profile existing = profiles.values().stream().parallel()
                .filter(p ->
                    p.getOwnerUuid().equals(owner.getUuid()) &&
                            p.getVersion().getType() == VersionType.SNAPSHOT &&
                            p.getVersion().isLatest())
                .findFirst().orElse(null);
        if (existing != null) {
            return;
        }

        Profile profile = new ProfileBuilder(
                "Latest Snapshot",
                versionManifest.getLatestSnapshot(),
                owner
        ).setLatest(true).create();
        profiles.put(profile).save();
    }

    /**
     * Creates and saves the 'Latest Forge' profile for the specified user.
     * @param owner The owner of this profile.
     * @throws IOException Thrown if something goes wrong.
     */
    public void makeLatestForgeProfile(User owner) throws IOException {
        // TODO: Make latest forge profile
    }

    /**
     * Prepares the appropriate version file for the given version info.
     * TODO: This needs to validate the files against their SHA1 signatures.
     *
     * @param feedback The {@link TaskFeedback} for this method.
     * @param info The {@link VersionInfo} to fetch the version from.
     * @return A new {@link Version}
     * @throws IOException Thrown if something goes wrong.
     */
    public Version downloadVersion(TaskFeedback<Version> feedback, @NotNull VersionInfo info) throws IOException {
        long stepsCompleted = 0;
        long maxSteps = 3;

        // Prepare Directories
        final Path VERSION_FOLDER = Version.getFolderPath(info.getId());
        final Path VERSION_FILE = VERSION_FOLDER.resolve(info.getId() + ".json");
        final Path JAR_FILE = Version.getFolderPath(info.getId()).resolve(info.getId() + ".jar");
        Files.createDirectories(VERSION_FOLDER);
        feedback.updateProgress(++stepsCompleted, maxSteps);

        // Download the version file or fetch it if already downloaded
        feedback.updateMessage("Checking for game version...");
        Version version;
        if (!Files.exists(VERSION_FILE, LinkOption.NOFOLLOW_LINKS)) {
            feedback.updateMessage("Downloading information for " + info.getId() + "...");
            version = Version.GSON.fromJson(Network.stringify(Network.fetch(new URL(info.getUrl()))), Version.class);
            Files.write(VERSION_FILE, Version.GSON.toJson(version).getBytes(), StandardOpenOption.CREATE, LinkOption.NOFOLLOW_LINKS);
            feedback.updateMessage("Download complete.");
        } else {
            feedback.updateMessage("Information file for " + info.getId() + " already exists.");
            version = Version.GSON.fromJson(Files.newBufferedReader(VERSION_FILE), Version.class);
        }
        feedback.updateProgress(++stepsCompleted, maxSteps);

        // Check if this task was cancelled
        if (feedback.isCancelled()) {
            throw new CancellationException();
        }

        // Download the jar file
        feedback.updateMessage("Checking for jar file...");
        if (!Files.exists(JAR_FILE, LinkOption.NOFOLLOW_LINKS)) {
            assert version.getDownloads().getClient().getUrl() != null; // This won't be null
            feedback.updateMessage("Downloading " + version.getId() + " jar file...");
            Network.download(new URL(version.getDownloads().getClient().getUrl()), JAR_FILE);
            feedback.updateMessage("Download complete.");
        }
        feedback.updateMessage("Jar file for " + version.getId() + " is ready.");
        feedback.updateProgress(++stepsCompleted, maxSteps);

        // Return the version
        return version;
    }
    public Version downloadVersion(@NotNull VersionInfo info) throws IOException {
        return this.downloadVersion(new DummyTaskFeedback<>(), info);
    }

    /**
     * Downloads the appropriate Java 8 version for the user's system.
     * TODO: This needs to validate the files against their SHA1 signatures.
     *
     * @param feedback The {@link TaskFeedback} for this method.
     * @return The {@link Path} to the Java folder
     * @throws IOException Thrown if something goes wrong.
     * @throws ArchiveException Thrown if extracting something goes wrong.
     */
    public Path downloadJava(TaskFeedback<Path> feedback) throws IOException, ArchiveException {
        long stepsCompleted = 0;
        long maxSteps = 5;

        // Prepare directories
        final Path JAVA_FOLDER_PATH = Path.of("launcher/runtime/jre-1.8");
        Files.createDirectories(JAVA_FOLDER_PATH);
        feedback.updateProgress(++stepsCompleted, maxSteps);

        // Fetch correct URL
        feedback.updateMessage("Recognizing system...");
        URL url;
        boolean isTarFile = false;
        switch (SystemInfo.OS_NAME) {
            case "windows":
                if (SystemInfo.OS_BIT.equals("32")) {
                    url = settings.getEndpoints().getJava8Win32();
                } else {
                    url = settings.getEndpoints().getJava8Win64();
                }
                break;
            case "mac":
                url = settings.getEndpoints().getJava8Mac();
                isTarFile = true;
                break;
            case "linux":
                url = settings.getEndpoints().getJava8Linux();
                isTarFile = true;
                break;
            default:
                throw new IOException("Unrecognized systems do not support auto-download of legacy Java.");
        }
        feedback.updateMessage("Found a supported Java version. You're running " + SystemInfo.OS_NAME + " x" + SystemInfo.OS_ARCH + ".");
        feedback.updateProgress(++stepsCompleted, maxSteps);

        // Download file
        feedback.updateMessage("Checking for existing file...");
        Path compressedPath = JAVA_FOLDER_PATH.resolve("jre-1.8" + (isTarFile ? ".tar.gz" : ".zip"));
        if (!Files.exists(compressedPath, LinkOption.NOFOLLOW_LINKS)) {
            feedback.updateMessage("Existing file not found. Downloading Java 8...");
            Network.download(url, compressedPath);
            feedback.updateMessage("Download complete.");
        }
        feedback.updateMessage("Java 8 compressed file exists.");
        feedback.updateProgress(++stepsCompleted, maxSteps);

        // Check if this task was cancelled
        if (feedback.isCancelled()) {
            throw new CancellationException();
        }

        // Extract file
        feedback.updateMessage("Checking for extracted files...");
        Path javaPath;
        if (!SystemInfo.OS_NAME.equals("windows")) {
            javaPath = JAVA_FOLDER_PATH.resolve("bin/java");
        } else {
            javaPath = JAVA_FOLDER_PATH.resolve("bin/java.exe");
        }
        if (!Files.exists(javaPath, LinkOption.NOFOLLOW_LINKS)) {
            feedback.updateMessage("Extracting...");

            // Create archive stream
            ArchiveInputStream archive;
            if (isTarFile) {
                // Extract tar from tar.gz
                Path tarPath = JAVA_FOLDER_PATH.resolve("jre-1.8.tar");
                if (!Files.exists(tarPath, LinkOption.NOFOLLOW_LINKS)) {
                    GZIPInputStream gzipInputStream = new GZIPInputStream(new BufferedInputStream(Files.newInputStream(compressedPath)));
                    Files.copy(gzipInputStream, tarPath);
                    gzipInputStream.close();
                }

                // Create archive
                archive = new ArchiveStreamFactory().createArchiveInputStream("tar", new BufferedInputStream(Files.newInputStream(tarPath)));
            } else {
                // Create archive
                archive = new ArchiveStreamFactory().createArchiveInputStream("zip", new BufferedInputStream(Files.newInputStream(compressedPath)));
            }

            // Extract files
            ArchiveEntry entry;
            while ((entry = archive.getNextEntry()) != null) {

                // Check if this task was cancelled
                if (feedback.isCancelled()) {
                    Files.deleteIfExists(javaPath);
                    throw new CancellationException();
                }

                // Extract file
                String path = entry.getName().substring(entry.getName().indexOf('/') + 1);
                Path entryPath = JAVA_FOLDER_PATH.resolve(path);
                feedback.updateMessage("Extracting " + entryPath.getFileName() + "...");
                if (!entry.isDirectory()) {
                    Files.createDirectories(entryPath.getParent());
                    Files.copy(archive, entryPath, StandardCopyOption.REPLACE_EXISTING);
                }
            }

            // Close archive
            archive.close();
            feedback.updateMessage("Extraction complete.");
        }
        feedback.updateProgress(++stepsCompleted, maxSteps);

        // Determine if the java file still doesn't exist, return if it still exists
        if (!Files.exists(javaPath, LinkOption.NOFOLLOW_LINKS)) {
            throw new IOException("Unable to find java location!");
        }
        feedback.updateMessage("All Java files readily available.");
        feedback.updateProgress(++stepsCompleted, maxSteps);
        return javaPath;
    }
    public Path downloadJava() throws IOException, ArchiveException {
        return downloadJava(new DummyTaskFeedback<>());
    }

    /**
     * Downloads the appropriate libraries for the provided {@link Version}.
     * TODO: This needs to validate the files against their SHA1 signatures.
     *
     * @param feedback The {@link TaskFeedback} for this method.
     * @param versionId The id of the {@link Version} to get the natives folder for
     * @param rawLibs An array of raw libraries to download
     * @return A {@link Library} array containing non-native-only libraries.
     * @throws IOException Thrown if something goes wrong.
     */
    public Library[] downloadLibraries(TaskAnonymousFeedback feedback, String versionId, Library[] rawLibs) throws IOException {
        // Prepare directories
        Path nativesFolderPath = Version.getFolderPath(versionId).resolve("natives/");
        Files.createDirectories(nativesFolderPath);

        // Filter libraries
        feedback.updateMessage("Filtering Libraries...");
        List<Library> libraries = Arrays.stream(rawLibs).filter(library -> {
            // If there are no rules, return true
            if (library.getRules() == null || library.getRules().length == 0) {
                return true;

            // If the rules resolve to allow, return true
            } else if (Rule.determineAction(library.getRules()) == Action.ALLOW) {
                return true;

            // Otherwise, return false
            } else {
                return false;
            }
        }).collect(Collectors.toUnmodifiableList());
        feedback.updateMessage("Libraries filtered!");

        // Main download loop
        long stepsCompleted = 0;
        long maxSteps = libraries.size();
        for (Library library : libraries) {
            // Check if this task was cancelled
            if (feedback.isCancelled()) {
                throw new CancellationException();
            }

            // Don't try and download libraries that don't have downloads
            if (library.getDownloads() == null) {
                continue;
            }

            // Download library jar
            Artifact jarArtifact = library.getDownloads().getArtifact();
            if (jarArtifact != null) {
                assert jarArtifact.getPath() != null; // This won't be null for a library jar

                // Check for empty URLs
                if (jarArtifact.getUrl() == null || jarArtifact.getUrl().isEmpty()) {
                    continue;
                }

                Path jarPath = Library.FOLDER_LOCATION.resolve(jarArtifact.getPath());
                feedback.updateMessage("Checking for library " + library.getNameDetails()[1] + "...");
                if (!Files.exists(jarPath, LinkOption.NOFOLLOW_LINKS)) {
                    feedback.updateMessage("Downloading library " + library.getNameDetails()[1] + "...");
                    Files.createDirectories(jarPath.getParent());
                    Network.download(new URL(jarArtifact.getUrl()), jarPath);
                }
            }

            // Check if this task was cancelled
            if (feedback.isCancelled()) {
                throw new CancellationException();
            }

            // Download and extract natives
            Artifact natArtifact = library.getTargetedNatives();
            if (natArtifact != null) {
                assert natArtifact.getPath() != null; // This won't be null for a native
                assert natArtifact.getUrl() != null; // This won't be null for a native

                Path natPath = Library.FOLDER_LOCATION.resolve(natArtifact.getPath());
                if (!Files.exists(natPath, LinkOption.NOFOLLOW_LINKS)) {
                    feedback.updateMessage("Downloading natives for " + library.getNameDetails()[1] + "...");
                    Files.createDirectories(natPath.getParent());
                    Network.download(new URL(natArtifact.getUrl()), natPath);
                }

                // Extract the native
                feedback.updateMessage("Extracting natives for " + library.getNameDetails()[1] + "...");
                this.extractNative(natPath, nativesFolderPath, library.getExtract() != null ? library.getExtract().get("exclude") : null);
            }
            feedback.updateProgress(++stepsCompleted, maxSteps);
        }
        feedback.updateMessage("All libraries downloaded and all natives extracted.");

        // Check if this task was cancelled
        if (feedback.isCancelled()) {
            throw new CancellationException();
        }

        // Filter the libraries to exclude any native-only libraries (so it only returns 'true' libraries)
        return libraries.stream().parallel()
                .filter(library ->
                        library.getName().toLowerCase().contains("forge") ||
                        (
                            library.getDownloads() != null &&
                            library.getDownloads().getArtifact() != null)
                        )
                .toArray(Library[]::new);
    }
    public Library[] downloadLibraries(TaskAnonymousFeedback feedback, Version version) throws IOException {
        return this.downloadLibraries(feedback, version.getId(), version.getLibraries());
    }
    public Library[] downloadLibraries(String versionId, Library[] rawLibs) throws IOException {
        return this.downloadLibraries(new DummyTaskFeedback<>(), versionId, rawLibs);
    }
    public Library[] downloadLibraries(Version version) throws IOException {
        return this.downloadLibraries(new DummyTaskFeedback<>(), version);
    }

    /**
     * Downloads all of the assets for the provided {@link Version}.
     * TODO: This needs to validate the files against their SHA1 signatures.
     *
     * @param feedback The {@link TaskFeedback} for this method.
     * @param version The {@link Version} to get assets for.
     * @param profile The {@link Profile} these assets are being downloaded with.
     * @return A new {@link AssetIndex}.
     * @throws IOException Thrown if something goes wrong.
     */
    public AssetIndex downloadAssets(TaskFeedback<AssetIndex> feedback, Version version, Profile profile) throws IOException {
        // Prepare directories (don't call them every loop)
        final Path OBJECTS_FOLDER_PATH = AssetIndex.FOLDER_LOCATION.resolve("objects/");
        final Path VIRTUAL_FOLDER_PATH = AssetIndex.FOLDER_LOCATION.resolve("virtual/legacy/");
        final Path RESOURCES_FOLDER_PATH = Path.of(profile.getPath()).resolve("resources/");
        final Path LOG_CONFIGS_FOLDER_PATH = AssetIndex.FOLDER_LOCATION.resolve("log_configs/");
        final Path INDEX_PATH = AssetIndex.getFilePath(version.getAssetIndex().getId());
        Files.createDirectories(LOG_CONFIGS_FOLDER_PATH);
        Files.createDirectories(INDEX_PATH.getParent());

        // Download the primary index file
        AssetIndex index;
        if (!Files.exists(INDEX_PATH)) {
            feedback.updateMessage("Downloading the asset index...");
            assert version.getAssetIndex().getUrl() != null; // This won't be null for an asset index
            index = AssetIndex.GSON.fromJson(Network.stringify(Network.fetch(new URL(version.getAssetIndex().getUrl()))), AssetIndex.class);
            index.setId(version.getAssetIndex().getId()).save();
        } else {
            feedback.updateMessage("Parsing asset index...");
            index = new AssetIndex(version.getAssetIndex().getId()).load();
        }
        feedback.updateMessage("Asset index loaded.");

        // Check if this task was cancelled
        if (feedback.isCancelled()) {
            throw new CancellationException();
        }

        // Create directories if needed
        if (index.isVirtual()) {
            Files.createDirectories(VIRTUAL_FOLDER_PATH);
        }
        if (index.isMapToResources()) {
            Files.createDirectories(RESOURCES_FOLDER_PATH);
        }

        // Main download loop
        long stepsCompleted = 0;
        long maxSteps = index.getObjects().entrySet().size() * 3L + 1; // 3 steps per asset, 1 log file download
        for (Map.Entry<String, Asset> entry : index.getObjects().entrySet()) {
            // Check if this task was cancelled
            if (feedback.isCancelled()) {
                throw new CancellationException();
            }

            // Download asset
            Asset asset = entry.getValue();
            String assetLocation = asset.getId() + "/" + asset.getHash();
            Path assetPath = OBJECTS_FOLDER_PATH.resolve(assetLocation);
            feedback.updateMessage("Checking for asset " + asset.getHash());
            if (!Files.exists(assetPath, LinkOption.NOFOLLOW_LINKS)) {
                feedback.updateMessage("Downloading asset " + asset.getHash() + "...");
                Files.createDirectories(assetPath.getParent());
                Network.download(new URL(settings.getEndpoints().getAssetApi() + assetLocation), assetPath);
            }
            feedback.updateProgress(++stepsCompleted, maxSteps);

            // Check if this task was cancelled
            if (feedback.isCancelled()) {
                throw new CancellationException();
            }

            // If the asset is virtual, copy the file to the virtual location
            if (index.isVirtual()) {
                Path assetPathVirtual = VIRTUAL_FOLDER_PATH.resolve(entry.getKey());
                if (!Files.exists(assetPathVirtual, LinkOption.NOFOLLOW_LINKS)) {
                    feedback.updateMessage("Copying asset to legacy...");
                    Files.createDirectories(assetPathVirtual.getParent());
                    Files.copy(assetPath, assetPathVirtual);
                }
            }
            feedback.updateProgress(++stepsCompleted, maxSteps);

            // Check if this task was cancelled
            if (feedback.isCancelled()) {
                throw new CancellationException();
            }

            // If map to resources, copy the file to the resources location
            if (index.isMapToResources()) {
                Path assetResourcePath = RESOURCES_FOLDER_PATH.resolve(entry.getKey());
                if (!Files.exists(assetResourcePath, LinkOption.NOFOLLOW_LINKS)) {
                    feedback.updateMessage("Copying asset to resources...");
                    Files.createDirectories(assetResourcePath.getParent());
                    Files.copy(assetPath, assetResourcePath);
                }
            }
            feedback.updateProgress(++stepsCompleted, maxSteps);
        }
        feedback.updateMessage("All assets downloaded.");

        // Download log files
        if (version.getLogging() != null) {
            Artifact artifact = version.getLogging().getClient().getFile();
            assert artifact.getId() != null; // This won't be null for a log file
            assert artifact.getUrl() != null; // This won't be null for a log file
            Path logFilePath = LOG_CONFIGS_FOLDER_PATH.resolve(artifact.getId());
            if (!Files.exists(logFilePath, LinkOption.NOFOLLOW_LINKS)) {
                feedback.updateMessage("Downloading " + version.getId() + " log configuration...");
                Network.download(new URL(artifact.getUrl()), logFilePath);
            }
        }
        feedback.updateProgress(++stepsCompleted, maxSteps);

        // Return the index
        return index;
    }
    public AssetIndex downloadAssets(Version version, Profile profile) throws IOException {
        return this.downloadAssets(new DummyTaskFeedback<>(), version, profile);
    }

    /**
     * Launches the game.
     *
     * @param feedback The {@link TaskFeedback} for this method.
     * @param user The {@link User} we are launching with.
     * @param profile The {@link Profile} that is being launched.
     * @param version The {@link Version} to use for launch.
     * @param libraries The {@link Library} array as provided by {@link ProtoLauncher#downloadLibraries}
     * @param assetIndex The {@link AssetIndex} as provided by {@link ProtoLauncher#downloadAssets}
     * @param javaPath The (optional) java path as provided by {@link ProtoLauncher#downloadJava}
     * @param launcherVersion The version of this launcher
     * @return A new {@link Process} for Minecraft
     * @throws IOException Thrown if something goes terribly wrong.
     */
    public Process launch(TaskFeedback<Process> feedback, User user, Profile profile,
                          Version version, Library[] libraries, AssetIndex assetIndex,
                          @Nullable Path javaPath, String launcherVersion) throws IOException {
        // Prepare Run Directory
        final Path RUN_FOLDER_PATH = Path.of(profile.getPath()).toAbsolutePath();
        Files.createDirectories(RUN_FOLDER_PATH);

        // Prepare jar location
        final Path JAR_LOCATION = Version.getFolderPath(version.getId()).resolve(version.getId() + ".jar");

        // Prepare classpath
        feedback.updateMessage("Preparing classpath...");
        String[] classpath = new String[libraries.length + 1];
        for (int i = 0; i < classpath.length - 1; i++) {
            classpath[i] = libraries[i].getLocation().toAbsolutePath().toString();
        }
        classpath[classpath.length - 1] = JAR_LOCATION.toAbsolutePath().toString();

        // Prepare launch arguments
        feedback.updateMessage("Preparing arguments...");
        String arguments = "";

        // Forge arguments
        if (profile.getVersion().getForge() != null) {
            arguments += "-Dforgewrapper.librariesDir=${fw_libsdir} ";
            arguments += "-Dforgewrapper.installer=${fw_installer} ";
            arguments += "-Dforgewrapper.minecraft=${fw_jar} ";
        }

        // Regular arguments
        if (version.getMinecraftArguments() != null) {
            arguments += "-Djava.library.path=${natives_directory} -cp ${classpath}" + ' ' + version.getMainClass() + ' ' + version.getMinecraftArguments();
        } else {
            arguments += version.getArguments().getJvm() + ' ' + version.getMainClass() + ' ' + version.getArguments().getGame();
        }

        // Replace argument variables
        arguments = arguments.replace("${auth_username}", user.getUsername());
        arguments = arguments.replace("${auth_player_name}", user.getUsername());
        arguments = arguments.replace("${version_name}", version.getId());
        arguments = arguments.replace("${game_directory}", '"' + RUN_FOLDER_PATH.toString() + '"');
        arguments = arguments.replace("${assets_root}", '"' + AssetIndex.FOLDER_LOCATION.toAbsolutePath().toString() + '"');
        assert version.getAssetIndex().getId() != null; // This won't be null for an asset index
        arguments = arguments.replace("${assets_index_name}", version.getAssetIndex().getId());
        if (assetIndex.isMapToResources()) {
            arguments = arguments.replace("${game_assets}", '"' + RUN_FOLDER_PATH.resolve("assets/").toAbsolutePath().toString() + '"');
        } else if (assetIndex.isVirtual()) {
            arguments = arguments.replace("${game_assets}", '"' + AssetIndex.FOLDER_LOCATION.resolve("virtual/legacy/").toString() + '"');
        } else {
            arguments = arguments.replace("${game_assets}", '"' + AssetIndex.FOLDER_LOCATION.toAbsolutePath().toString() + '"');
        }
        arguments = arguments.replace("${auth_uuid}", user.getUuid());
        arguments = arguments.replace("${auth_access_token}", user.getAccessToken());
        arguments = arguments.replace("${auth_session}", "token:" + user.getAccessToken() + ":" + user.getUuid());
        arguments = arguments.replace("${user_type}", "mojang");
        arguments = arguments.replace("${user_properties}", user.getUserProperties());
        arguments = arguments.replace("${version_type}", version.getVersionType().toString().toLowerCase());
        arguments = arguments.replace("${natives_directory}", '"' + Version.getFolderPath(version.getId()).resolve("natives/").toAbsolutePath().toString() + '"');
        arguments = arguments.replace("${launcher_name}", "ProtoLauncher");
        arguments = arguments.replace("${launcher_version}", launcherVersion);
        arguments = arguments.replace("${classpath}", '"' + String.join(";", classpath) + '"');

        // Replace forge argument variables
        if (profile.getVersion().getForge() != null) {
            arguments = arguments.replace("${fw_libsdir}", '"' + Library.FOLDER_LOCATION.toAbsolutePath().toString() + '"');
            arguments = arguments.replace("${fw_installer}", '"' + Path.of("mojang/libraries/net/minecraftforge/installers/forge-" + profile.getVersion().getMinecraft() + "-" + profile.getVersion().getForge() + "-installer.jar").toAbsolutePath().toString() + '"');
            arguments = arguments.replace("${fw_jar}", '"' + JAR_LOCATION.toAbsolutePath().toString() + '"');
        }

        // Add resolution arguments
        if (profile.getLaunchSettings().getGameResolutionX() != -1) {
            arguments += " --width=" + profile.getLaunchSettings().getGameResolutionX();
        }
        if (profile.getLaunchSettings().getGameResolutionY() != -1) {
            arguments += " --height=" + profile.getLaunchSettings().getGameResolutionY();
        }

        // Add JVM arguments
        if (profile.getLaunchSettings().getJvmArguments() != null) {
            arguments = profile.getLaunchSettings().getJvmArguments() + " " + arguments;
        }

        // Prepare the launch command
        feedback.updateMessage("Preparing command...");
        String command;
        if (javaPath == null) {
            command = "java -Xdiag " + arguments;
        } else {
            command = javaPath + " -Xdiag " + arguments;
        }

        // Launch the game
        feedback.updateMessage("Launching...");
        ProcessBuilder builder = new ProcessBuilder(command.split(" "));
        builder.directory(RUN_FOLDER_PATH.toFile());
        return builder.inheritIO().start();
    }
    public Process launch(User user, Profile profile, Version version,
                          Library[] libraries, AssetIndex assetIndex,
                          @Nullable Path javaPath, String launcherVersion) throws IOException {
        return this.launch(new DummyTaskFeedback<>(), user, profile, version, libraries, assetIndex, javaPath, launcherVersion);
    }

    /**
     * Injects Minecraft Forge into the specified profile.<br/><br/>
     * Should be called before downloading libraries.
     *
     * @param feedback The {@link TaskFeedback} for this method.
     * @param version The {@link Version} to clone for injection.
     * @param profile The {@link Profile} that is having forge added to it
     * @return The modified {@link Version} with forge injected
     * @throws IOException Thrown if something goes wrong
     */
    public Version injectForge(TaskFeedback<Version> feedback, Version version, Profile profile) throws IOException {
        // Ensure profile is forge compatible
        if (profile.getVersion().getForge() == null || profile.getVersion().getFabric() != null) {
            throw new IOException("This profile is not set up for forge. Why are we trying to inject forge to it?");
        }

        // Download Forge Wrapper
        if (!Files.exists(FORGE_WRAPPER, LinkOption.NOFOLLOW_LINKS)) {
            Files.createDirectories(FORGE_WRAPPER.getParent());
            Network.download(settings.getEndpoints().getForgeWrapper(), FORGE_WRAPPER);
        }

        // Get Forge Version Info
        ForgeVersionInfo fvi = forgeVersionManifest.getVersion(profile.getVersion().getMinecraft(), profile.getVersion().getForge());
        if (fvi == null) {
            throw new IOException("Failed to fetch the correct ForgeVersionInfo!");
        }

        // Modify Version Id
        // NOTE: This does NOT save. Do not MAKE it save. Important for version checking below.
        String oldVersionId = version.getId();
        version.setId(version.getId() + "-forge-" + fvi.getId());

        // Prepare Directories
        final Path VERSION_FOLDER = Version.getFolderPath(version.getId());
        final Path VERSION_FILE = VERSION_FOLDER.resolve(version.getId() + ".json");

        // Check for existing version file
        feedback.updateMessage("Checking for existing version file...");
        if (Files.exists(VERSION_FILE, LinkOption.NOFOLLOW_LINKS)) {
            // Okay.. this profile already supports this version of forge. What? How did we get here?
            feedback.updateMessage("Existing version file... we're done here.");
            return Version.GSON.fromJson(Files.newBufferedReader(VERSION_FILE), Version.class);
        }
        feedback.updateMessage("No existing version found. Creating one...");

        // Create Directories
        final Path INSTALLER_FILE = Path.of("mojang/libraries/net/minecraftforge/installers/" + fvi.getFilename());
        Files.createDirectories(VERSION_FOLDER);
        Files.createDirectories(INSTALLER_FILE.getParent());

        // Download installer
        feedback.updateMessage("Checking for forge installer...");
        if (!Files.exists(INSTALLER_FILE, LinkOption.NOFOLLOW_LINKS)) {
            feedback.updateMessage("Prompting for forge installer download...");
            CompletableFuture<URL> future = ForgeVersionManifest.promptForgeDownload(null, fvi.getUrl());
            URL installerUrl;
            try {
                installerUrl = future.get();
            } catch (Exception e) {
                throw new IOException("Installer download cancelled!");
            }
            feedback.updateMessage("Downloading forge installer...");
            Network.download(installerUrl, INSTALLER_FILE);
        }
        feedback.updateMessage("Installer ready.");

        // Create temporary directory
        Path temp = Files.createTempDirectory("pl_forge_install");

        // Copy install file to temporary directory
        Files.copy(INSTALLER_FILE.toAbsolutePath(), temp.resolve(INSTALLER_FILE.getFileName()));

        // Run ForgeWrapper Converter
        ProcessBuilder builder = new ProcessBuilder((
                "java -jar " + FORGE_WRAPPER.toAbsolutePath().toString() + " " +
                        "--installer=" + temp.resolve(fvi.getFilename()).toAbsolutePath().toString()
        ).split(" "));
        try {
            builder.directory(temp.toFile()).inheritIO().start().waitFor();
        } catch (InterruptedException e) {
            throw new IOException(e);
        }

        // Copy installer file
        final Path INSTALLER_FILE_LIB = Library.FOLDER_LOCATION.resolve("./net/minecraftforge/forge/" + fvi.getMcv() + "-" + fvi.getId() + "/" + fvi.getFilename());
        if (!Files.exists(INSTALLER_FILE_LIB, LinkOption.NOFOLLOW_LINKS)) {
            Files.createDirectories(INSTALLER_FILE_LIB.getParent());
            Files.copy(INSTALLER_FILE, INSTALLER_FILE_LIB, LinkOption.NOFOLLOW_LINKS);
        }

        // Copy wrapper
        final Path WRAPPER_FILE_LIB = Library.FOLDER_LOCATION.resolve("./io/github/zekerzhayard/ForgeWrapper/" + FORGE_WRAPPER.getFileName().toString());
        if (!Files.exists(WRAPPER_FILE_LIB, LinkOption.NOFOLLOW_LINKS)) {
            Files.createDirectories(WRAPPER_FILE_LIB.getParent());
            Files.copy(FORGE_WRAPPER, WRAPPER_FILE_LIB, LinkOption.NOFOLLOW_LINKS);
        }

        // Inject patches
        JsonObject obj = Version.GSON.fromJson(Files.readString(temp.resolve("forge-" + fvi.getMcv() + "-" + fvi.getId() + "/patches/net.minecraftforge.json").toAbsolutePath()), JsonObject.class);

        // Parse GSON
        Version forgeVersion = Version.GSON.fromJson(obj, Version.class);

        // Merge version
        version = version.merge(forgeVersion, false);

        // Save version
        version.save();

        // Download mavenFiles
        this.downloadLibraries(feedback, version.getId(), Version.GSON.fromJson(obj.getAsJsonObject().getAsJsonArray("mavenFiles"), Library[].class));

        // Copy client jar
        final Path OLD_CLIENT_JAR = Version.getFolderPath(oldVersionId).resolve(oldVersionId + ".jar");
        final Path NEW_CLIENT_JAR = Version.getFolderPath(version.getId()).resolve(version.getId() + ".jar");
        Files.copy(OLD_CLIENT_JAR, NEW_CLIENT_JAR, LinkOption.NOFOLLOW_LINKS);

        // Done!
        return version;
    }

    /**
     * Injects Minecraft Fabric into the specified profile.<br/><br/>
     * Should be called before downloading libraries.
     *
     * @param feedback The {@link TaskFeedback} for this method.
     * @param version The {@link Version} to clone for injection.
     * @param profile The {@link Profile} that is having fabric added to it
     * @return The modified {@link Version} with fabric injected
     * @throws IOException Thrown if something goes wrong
     */
    public Version injectFabric(TaskFeedback<Version> feedback, Version version, Profile profile) throws IOException {
        // Ensure profile is fabric compatible
        if (profile.getVersion().getFabric() == null || profile.getVersion().getForge() != null) {
            throw new IOException("This profile is not set up for fabric. Why are we trying to inject fabric to it?");
        }

        // Get Forge Version Info
        FabricVersionInfo fvi = fabricVersionManifest.getVersion(profile.getVersion().getMinecraft(), profile.getVersion().getFabric());
        if (fvi == null) {
            throw new IOException("Failed to fetch the correct ForgeVersionInfo!");
        }

        // Modify Version Id
        // NOTE: This does NOT save. Do not MAKE it save. Important for version checking below.
        String oldVersionId = version.getId();
        version.setId(version.getId() + "-fabric-" + fvi.getId());

        // Prepare Directories
        final Path VERSION_FOLDER = Version.getFolderPath(version.getId());
        final Path VERSION_FILE = VERSION_FOLDER.resolve(version.getId() + ".json");

        // Check for existing version file
        feedback.updateMessage("Checking for existing version file...");
        if (Files.exists(VERSION_FILE, LinkOption.NOFOLLOW_LINKS)) {
            // Okay.. this profile already supports this version of fabric. What? How did we get here?
            feedback.updateMessage("Existing version file... we're done here.");
            return Version.GSON.fromJson(Files.newBufferedReader(VERSION_FILE), Version.class);
        }
        feedback.updateMessage("No existing version found. Creating one...");

        // Download fabric version file as object
        JsonObject obj = Version.GSON.fromJson(Network.stringify(Network.fetch(new URL(fvi.getUrl()))), JsonObject.class);

        // Fix all libraries because for some reason fabric doesn't want to be normal
        JsonArray libArr = obj.getAsJsonArray("libraries");
        for (int i = 0; i < libArr.size(); i++) {
            JsonObject libObj = libArr.get(i).getAsJsonObject();

            // Get name and url
            String libName = libObj.get("name").getAsString();
            String libUrl = libObj.get("url").getAsString();

            // Construct actual URL
            String[] details = libName.split(":");
            String fullPath = details[0].replace(".", "/") + "/" + details[1].replace(":", "/") + "/" + details[2] + "/" + details[1] + "-" + details[2] + ".jar";
            String fullUrl = libUrl + fullPath;

            // Construct a new artifact
            JsonObject downloads = new JsonObject();
            JsonObject artifact = new JsonObject();
            artifact.addProperty("url", fullUrl);
            artifact.addProperty("path", fullPath);
            downloads.add("artifact", artifact);
            downloads.add("classifiers", null);

            // Update library object
            libObj.add("downloads", downloads);
            libObj.add("extract", null);
            libObj.add("natives", null);
            libObj.add("rules", null);
            libObj.remove("url");
        }
        obj.add("libraries", libArr);

        // Parse version
        Version fabricVersion = Version.GSON.fromJson(obj, Version.class);

        // Merge version
        version = version.merge(fabricVersion, true);

        // Save version
        version.save();

        // Copy client jar
        final Path OLD_CLIENT_JAR = Version.getFolderPath(oldVersionId).resolve(oldVersionId + ".jar");
        final Path NEW_CLIENT_JAR = Version.getFolderPath(version.getId()).resolve(version.getId() + ".jar");
        Files.copy(OLD_CLIENT_JAR, NEW_CLIENT_JAR, LinkOption.NOFOLLOW_LINKS);

        // Done!
        return version;
    }

    /**
     * Takes in a source jar file and extracts it to the destination path avoiding the exclusions list.
     *
     * @param source The source jar file
     * @param destination The destination directory
     * @param exclusions A list of file exclusions
     */
    private void extractNative(Path source, Path destination, @Nullable String[] exclusions) throws IOException {
        // Prepare jar file
        JarFile jar = new JarFile(source.toFile());
        Enumeration<JarEntry> entries = jar.entries();

        // Main extract loop
        while (entries.hasMoreElements()) {
            JarEntry entry = entries.nextElement();

            // Ignore non-dll files
            if (!entry.getName().endsWith(".dll")) {
                continue;
            }

            // Handle edge case where lwjgl.dll doesn't work with MC 1.16 if we don't remove the 64-bit version on a 32-bit PC
            if (SystemInfo.OS_ARCH.equals("x86") && entry.getName().equals("lwjgl.dll")) {
                continue;
            }

            // Create entry file location
            Path file = Paths.get(destination.toString(), entry.getName());

            // If the native already exists, ignore it and continue on
            if (Files.exists(file, LinkOption.NOFOLLOW_LINKS)) {
                continue;
            }

            // Process exclusions
            if (exclusions != null && exclusions.length > 0) {
                boolean exclude = false;
                for (String exclusion : exclusions) {
                    Path excludedFile = Path.of(exclusion);

                    // Compare the excluded filename to the jar file name and if they're equal don't extract
                    if (file.getFileName().equals(excludedFile.getFileName())) {
                        exclude = true;
                        break;

                        // Compare the excluded file parent's folder name to the jar file's folder name and if they're equal don't extract
                    } else if (file.getParent() != null && file.getParent().getFileName().equals(excludedFile.getFileName())) {
                        exclude = true;
                        break;
                    }
                }
                if (exclude) {
                    continue;
                }
            }

            // Copy native to destination
            Files.copy(jar.getInputStream(entry), file);
        }

        // Close jar file
        jar.close();
    }

    // Getters
    public static ProtoLauncher getInstance() {
        return instance;
    }
    public boolean isInitialized() {
        return initialized;
    }
    public Settings getSettings() {
        return settings;
    }
    public Store<User> getUsers() {
        return users;
    }
    public Store<Profile> getProfiles() {
        return profiles;
    }
    public VersionManifest getVersionManifest() {
        return versionManifest;
    }
    public ForgeVersionManifest getForgeVersionManifest() {
        return forgeVersionManifest;
    }
    public FabricVersionManifest getFabricVersionManifest() {
        return fabricVersionManifest;
    }
    public Yggdrasil getYggdrasil() {
        return yggdrasil;
    }
    public MicrosoftAuth getMicrosoftAuth() {
        return microsoftAuth;
    }

}