package net.protolauncher.api;

import com.google.gson.Gson;
import com.google.gson.annotations.Since;
import net.protolauncher.api.exception.NotInitializedException;
import net.protolauncher.util.ISaveable;
import net.protolauncher.util.Store;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.util.UUID;

public class Settings implements ISaveable {

    // Constants
    public static final double FILE_VERSION;
    public static final Path FILE_LOCATION;
    public static final Gson GSON;

    // Static Initializer
    static {
        FILE_VERSION = 1.00;
        FILE_LOCATION = Path.of("launcher/settings.json");
        GSON = ProtoLauncher.GSON.newBuilder().setVersion(FILE_VERSION).create();
    }

    // Properties
    @Since(1.00)
    private String clientToken;

    @Since(1.00)
    @Nullable
    private String currentUserUuid;

    @Since(1.00)
    @Nullable
    private String currentProfileUuid;

    @Since(1.00)
    private String lastManifestUpdate;

    @Since(1.00)
    private String maxManifestAge;

    @Since(1.00)
    private Endpoints endpoints;

    /**
     * Constructs new settings with no defaults.
     */
    public Settings() { }

    /**
     * Resets these settings to their default values.
     * @return These {@link Settings}
     */
    public Settings reset(@Nullable String clientToken, @Nullable String currentUserUuid, @Nullable String currentProfileUuid) {
        this.clientToken = clientToken != null ? clientToken : UUID.randomUUID().toString().replace("-", "");
        this.currentUserUuid = currentUserUuid;
        this.currentProfileUuid = currentProfileUuid;
        this.lastManifestUpdate = Instant.MIN.toString();
        this.maxManifestAge = Duration.ofHours(12).toString();
        this.endpoints = new Endpoints();
        return this;
    }

    /**
     * Resets these settings to their default values and generates a new clientToken.
     * @return These {@link Settings}
     */
    public Settings reset() {
        return this.reset(null, null, null);
    }

    /**
     * Determines whether or not this settings has a current user.
     * @return True if it does, otherwise false.
     */
    public boolean hasCurrentUser() {
        return currentUserUuid != null;
    }

    /**
     * Determines whether or not this settings has a current profile.
     * @return True if it does, otherwise false.
     */
    public boolean hasCurrentProfile() {
        return currentProfileUuid != null;
    }

    // Implement ISaveable
    @Override
    public Type getType() {
        return Settings.class;
    }
    @Override
    public Gson getGson() {
        return GSON;
    }
    @Override
    public Path getLocation() {
        return FILE_LOCATION;
    }

    // Getters
    public String getClientToken() {
        return clientToken;
    }
    public @Nullable String getCurrentUserUuid() {
        return currentUserUuid;
    }
    public @Nullable String getCurrentProfileUuid() {
        return currentProfileUuid;
    }
    public @Nullable User getCurrentUser() throws NotInitializedException {
        ProtoLauncher launcher = ProtoLauncher.getInstance();
        if (!launcher.isInitialized()) {
            throw new NotInitializedException();
        }
        return getCurrentUser(launcher.getUsers());
    }
    public @Nullable User getCurrentUser(Store<User> users) {
        return currentUserUuid == null ? null : users.getOrDefault(currentUserUuid, null);
    }
    public @Nullable Profile getCurrentProfile() throws NotInitializedException {
        ProtoLauncher launcher = ProtoLauncher.getInstance();
        if (!launcher.isInitialized()) {
            throw new NotInitializedException();
        }
        return getCurrentProfile(launcher.getProfiles());
    }
    public @Nullable Profile getCurrentProfile(Store<Profile> profiles) {
        return currentProfileUuid == null ? null : profiles.getOrDefault(currentProfileUuid, null);
    }
    public Instant getLastManifestUpdate() {
        return Instant.parse(lastManifestUpdate);
    }
    public Duration getMaxManifestAge() {
        return Duration.parse(maxManifestAge);
    }
    public Endpoints getEndpoints() {
        return endpoints;
    }

    // Setters
    public Settings setCurrentUserUuid(String currentUserUuid) {
        this.currentUserUuid = currentUserUuid;
        return this;
    }
    public Settings setCurrentProfileUuid(String currentProfileUuid) {
        this.currentProfileUuid = currentProfileUuid;
        return this;
    }
    public Settings setCurrentUser(@Nullable User currentUser) {
        this.currentUserUuid = currentUser == null ? null : currentUser.getUuid();
        return this;
    }
    public Settings setCurrentProfile(@Nullable Profile currentProfile) {
        this.currentProfileUuid = currentProfile == null ? null : currentProfile.getUuid();
        return this;
    }
    public Settings setLastManifestUpdate(Instant lastManifestUpdate) {
        this.lastManifestUpdate = lastManifestUpdate.toString();
        return this;
    }
    public Settings setMaxManifestAge(Duration maxManifestAge) {
        this.maxManifestAge = maxManifestAge.toString();
        return this;
    }

    // Stringify
    @Override
    public String toString() {
        return "Settings{" +
                "clientToken='" + clientToken + '\'' +
                ", currentUserUuid='" + currentUserUuid + '\'' +
                ", currentProfileUuid='" + currentProfileUuid + '\'' +
                ", lastManifestUpdate='" + lastManifestUpdate + '\'' +
                ", maxManifestAge='" + maxManifestAge + '\'' +
                ", endpoints=" + endpoints +
                '}';
    }

    // Sub-Objects
    public static class Endpoints {

        // Properties
        @Since(1.00)
        private URL yggdrasilApi;

        @Since(1.00)
        private MicrosoftApi microsoftApi;

        @Since(1.00)
        private URL assetApi;

        @Since(1.00)
        private URL avatarApi;

        @Since(1.00)
        private URL manifest;

        @Since(1.00)
        private URL forgeManifest;

        @Since(1.00)
        private URL fabricManifest;

        @Since(1.00)
        private URL forgeWrapper;

        @Since(1.00)
        private URL java8Win32;

        @Since(1.00)
        private URL java8Win64;

        @Since(1.00)
        private URL java8Mac;

        @Since(1.00)
        private URL java8Linux;

        /**
         * Constructs new endpoints with default values.
         */
        private Endpoints() {
            try {
                this.yggdrasilApi = new URL("https://authserver.mojang.com/");
                this.microsoftApi = new MicrosoftApi();
                this.assetApi = new URL("https://resources.download.minecraft.net/");
                this.avatarApi = new URL("https://crafatar.com/avatars/%uuid%?size=256&default=MHF_Steve&overlay");
                this.manifest = new URL("https://launchermeta.mojang.com/mc/game/version_manifest.json");
                this.forgeManifest = new URL("https://protolauncher.net/api/forge/forge_manifest.json");
                this.fabricManifest = new URL("https://protolauncher.net/api/fabric/fabric_manifest.json");
                this.forgeWrapper = new URL("https://github.com/ZekerZhayard/ForgeWrapper/releases/download/1.4.2/ForgeWrapper-1.4.2.jar");
                this.java8Win32 = new URL("https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u292-b10/OpenJDK8U-jre_x86-32_windows_hotspot_8u292b10.zip");
                this.java8Win64 = new URL("https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u292-b10/OpenJDK8U-jre_x64_windows_hotspot_8u292b10.zip");
                this.java8Mac = new URL("https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u292-b10/OpenJDK8U-jre_x64_mac_hotspot_8u292b10.tar.gz");
                this.java8Linux = new URL("https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u292-b10/OpenJDK8U-jre_x64_linux_hotspot_8u292b10.tar.gz");
            } catch (MalformedURLException e) {
                // Somebody really screwed up if this happens.
                e.printStackTrace();
                System.exit(1);
            }
        }

        // Getters
        public URL getYggdrasilApi() {
            return yggdrasilApi;
        }
        public MicrosoftApi getMicrosoftApi() {
            return microsoftApi;
        }
        public URL getAssetApi() {
            return assetApi;
        }
        public URL getAvatarApi() {
            return avatarApi;
        }
        public URL getManifest() {
            return manifest;
        }
        public URL getForgeManifest() {
            return forgeManifest;
        }
        public URL getFabricManifest() {
            return fabricManifest;
        }
        public URL getForgeWrapper() {
            return forgeWrapper;
        }
        public URL getJava8Win32() {
            return java8Win32;
        }
        public URL getJava8Win64() {
            return java8Win64;
        }
        public URL getJava8Mac() {
            return java8Mac;
        }
        public URL getJava8Linux() {
            return java8Linux;
        }

        // Stringify
        @Override
        public String toString() {
            return "Endpoints{" +
                    "yggdrasilApi=" + yggdrasilApi +
                    ", microsoftApi=" + microsoftApi +
                    ", assetApi=" + assetApi +
                    ", avatarApi=" + avatarApi +
                    ", manifest=" + manifest +
                    ", forgeManifest=" + forgeManifest +
                    ", fabricManifest=" + fabricManifest +
                    ", forgeWrapper=" + forgeWrapper +
                    ", java8Win32=" + java8Win32 +
                    ", java8Win64=" + java8Win64 +
                    ", java8Mac=" + java8Mac +
                    ", java8Linux=" + java8Linux +
                    '}';
        }

        // Sub-Objects
        public static class MicrosoftApi {

            // Properties
            @Since(1.00)
            private String clientId;

            @Since(1.00)
            private URL redirectUrl;

            @Since(1.00)
            private URL oauthAuthorizeUrl;

            @Since(1.00)
            private URL oauthTokenUrl;

            @Since(1.00)
            private URL xblUrl;

            @Since(1.00)
            private URL xstsUrl;

            @Since(1.00)
            private URL mcsUrl;

            /**
             * Constructs a new MicrosoftApi with default values.
             */
            private MicrosoftApi() {
                try {
                    this.clientId = "570b4885-8053-4442-a511-c6cc8df24dcb";
                    this.redirectUrl = new URL("https://login.microsoftonline.com/common/oauth2/nativeclient");
                    this.oauthAuthorizeUrl = new URL("https://login.live.com/oauth20_authorize.srf");
                    this.oauthTokenUrl = new URL("https://login.live.com/oauth20_token.srf");
                    this.xblUrl = new URL("https://user.auth.xboxlive.com/user/authenticate");
                    this.xstsUrl = new URL("https://xsts.auth.xboxlive.com/xsts/authorize");
                    this.mcsUrl = new URL("https://api.minecraftservices.com/");
                } catch (MalformedURLException e) {
                    // Somebody really screwed up if this happens.
                    e.printStackTrace();
                    System.exit(1);
                }
            }

            // Getters
            public String getClientId() {
                return clientId;
            }
            public URL getRedirectUrl() {
                return redirectUrl;
            }
            public URL getOauthAuthorizeUrl() {
                return oauthAuthorizeUrl;
            }
            public URL getOauthTokenUrl() {
                return oauthTokenUrl;
            }
            public URL getXblUrl() {
                return xblUrl;
            }
            public URL getXstsUrl() {
                return xstsUrl;
            }
            public URL getMcsUrl() {
                return mcsUrl;
            }

            // Stringify
            @Override
            public String toString() {
                return "MicrosoftApi{" +
                        "clientId='" + clientId + '\'' +
                        ", redirectUrl=" + redirectUrl +
                        ", oauthAuthorizeUrl=" + oauthAuthorizeUrl +
                        ", oauthTokenUrl=" + oauthTokenUrl +
                        ", xblUrl=" + xblUrl +
                        ", xstsUrl=" + xstsUrl +
                        ", mcsUrl=" + mcsUrl +
                        '}';
            }

        }

    }

}