package net.protolauncher.api;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Since;
import com.google.gson.reflect.TypeToken;
import javafx.stage.Stage;
import javafx.util.Pair;
import net.protolauncher.api.exception.NotInitializedException;
import net.protolauncher.mojang.auth.MicrosoftAuth;
import net.protolauncher.mojang.auth.MicrosoftAuth.MicrosoftResponse;
import net.protolauncher.mojang.auth.MicrosoftAuth.MinecraftResponse;
import net.protolauncher.mojang.auth.MicrosoftAuth.XboxLiveResponse;
import net.protolauncher.mojang.auth.Yggdrasil;
import net.protolauncher.util.IStorable;
import net.protolauncher.util.Network;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.HashMap;

public class User implements IStorable {

    // Constants
    public static final double STORE_VERSION;
    public static final Path STORE_LOCATION;
    public static final Gson GSON;

    // Static Initializer
    static {
        STORE_VERSION = 1.00;
        STORE_LOCATION = Path.of("launcher/users.json");
        GSON = ProtoLauncher.GSON.newBuilder().setVersion(STORE_VERSION).create();
    }

    // Properties
    @Since(1.00)
    private String username;

    @Since(1.00)
    private String uuid;

    @Since(1.00)
    private String userProperties;

    @Since(1.00)
    private String accessToken;

    @Since(1.00)
    @Nullable
    private MicrosoftInfo microsoftInfo;

    /**
     * Constructs a new user.
     *
     * @param username The Minecraft username
     * @param uuid The Minecraft uuid
     * @param userProperties Used for Minecraft 1.7
     * @param accessToken A Minecraft access token used for authentication
     * @param microsoftInfo Information about the verification for Microsoft logins
     */
    private User(String username, String uuid, String userProperties, String accessToken, @Nullable MicrosoftInfo microsoftInfo) {
        this.username = username;
        this.uuid = uuid;
        this.userProperties = userProperties;
        this.accessToken = accessToken;
        this.microsoftInfo = microsoftInfo;
    }

    /**
     * Logs in and creates a new user using the (legacy) Mojang method.
     *
     * @param username The username to login with
     * @param password The password to login with
     * @return A new {@link User}
     * @throws Exception Thrown if something goes wrong during the login process
     */
    public static User loginMojang(String username, String password) throws Exception {
        // Fetch Launcher & Validate Initialization
        ProtoLauncher launcher = ProtoLauncher.getInstance();
        if (!launcher.isInitialized()) {
            throw new NotInitializedException();
        }

        // Fetch Yggdrasil Response
        Yggdrasil.Response response = launcher.getYggdrasil().authenticate(username, password);
        if (response.getError() != null) {
            throw new Exception(response.getErrorMessage());
        }

        // Parse Properties
        String properties;
        if (response.getUser() != null && response.getUser().getProperties() != null) {
            properties = deserializeProperties(response.getUser().getProperties());
        } else {
            properties = "{}";
        }

        // Create new user
        return new User(
            response.getSelectedProfile().getName(),
            response.getSelectedProfile().getId(),
            properties,
            response.getAccessToken(),
            null
        );
    }

    /**
     * Logs in and creates a new user using the Microsoft method.
     *
     * @param authCode The authCode to login with
     * @return A new {@link User}
     * @throws Exception Thrown if something goes wrong during the login process
     * @see MicrosoftAuth#promptMicrosoftLogin(Stage)
     */
    public static User loginMicrosoft(String authCode) throws Exception {
        // Fetch Launcher & Validate Initialization
        ProtoLauncher launcher = ProtoLauncher.getInstance();
        if (!launcher.isInitialized()) {
            throw new NotInitializedException();
        }
        MicrosoftAuth mca = launcher.getMicrosoftAuth();

        // Authenticate
        MicrosoftResponse micr = mca.authenticateMicrosoft(authCode);
        if (micr.getError() != null) {
            throw new IOException("Internal error while authenticating with Microsoft! " + micr.getErrorDescription());
        }
        Pair<MicrosoftInfo, MinecraftResponse> pair = authMicrosoft(mca, micr.getAccessToken(), micr.getRefreshToken());
        MicrosoftInfo info = pair.getKey();
        MinecraftResponse mcr = pair.getValue();

        // Ensure Game Ownership
        if (!mca.verifyOwnership(mcr.getAccessToken())) {
            throw new Exception("User does not own the game!");
        }

        // Get Profile
        MicrosoftAuth.Profile profile = mca.getProfile(mcr.getAccessToken());

        // Create New User
        return new User(
            profile.getName(),
            profile.getId(),
            "{}",
            mcr.getAccessToken(),
            info
        );
    }

    /**
     * Authenticates a Minecraft account using the Microsoft accessToken and refreshToken
     *
     * @param mca An instance of {@link MicrosoftAuth}
     * @param accessToken The Microsoft accessToken
     * @param refreshToken The Microsoft refreshToken
     * @return A {@link Pair} containing a {@link MicrosoftInfo} as the key and {@link MinecraftResponse} as the value
     * @throws IOException Thrown if something goes wrong while authenticating
     */
    private static Pair<MicrosoftInfo, MinecraftResponse> authMicrosoft(MicrosoftAuth mca, String accessToken, String refreshToken) throws IOException {
        // Authenticate Xbox Live
        XboxLiveResponse xblr;
        try {
            xblr = mca.authenticateXboxLive(accessToken);
        } catch (IOException e) {
            if (e.getMessage().startsWith("401")) {
                MicrosoftResponse mcr = mca.refreshMicrosoft(refreshToken);
                if (mcr.getError() != null) {
                    throw new IOException("Internal error while refreshing the access token! " + mcr.getErrorDescription());
                } else {
                    accessToken = mcr.getAccessToken();
                    xblr = mca.authenticateXboxLive(accessToken);
                }
            } else {
                throw e;
            }
        }
        String uhs = xblr.getDisplayClaims().getXui()[0].getUhs();

        // Authenticate XSTS
        XboxLiveResponse xstsr = mca.authenticateXsts(xblr.getToken());

        // Authenticate Minecraft
        MinecraftResponse mcr = mca.authenticateMinecraft(xstsr.getToken(), uhs);

        // Create Info
        MicrosoftInfo info = new MicrosoftInfo(
            accessToken,
            refreshToken,
            xblr.getToken(),
            uhs,
            xstsr.getToken(),
            System.currentTimeMillis() + (Long.parseLong(mcr.getExpiresIn()) * 1000 * 60)
        );

        // Return
        return new Pair<>(info, mcr);
    }

    /**
     * Checks if this user's access token is still valid.
     * If not, it will attempt to refresh it.
     *
     * @return Whether or not the access token is valid.
     * @throws NotInitializedException Thrown if the launcher is not initialized.
     * @throws IOException Thrown if something goes wrong.
     */
    public boolean validate() throws NotInitializedException, IOException {
        // Fetch Launcher & Validate Initialization
        ProtoLauncher launcher = ProtoLauncher.getInstance();
        if (!launcher.isInitialized()) {
            throw new NotInitializedException();
        }

        // Validate
        if (microsoftInfo != null) {
            return this.validateMicrosoft(launcher.getMicrosoftAuth());
        } else {
            return this.validateMojang(launcher.getYggdrasil());
        }
    }
    private boolean validateMojang(Yggdrasil yggdrasil) throws IOException {
        boolean valid = yggdrasil.validate(accessToken);
        if (!valid) {
            Yggdrasil.Response response = yggdrasil.refresh(accessToken);
            if (response.getError() != null && response.getError().equals("TooManyRequestsException")) {
                throw new IOException("TooManyRequestsException");
            } else if (response.getError() != null) {
                return false;
            } else {
                accessToken = response.getAccessToken();
                return true;
            }
        } else {
            return true;
        }
    }
    private boolean validateMicrosoft(@NotNull MicrosoftAuth mca) {
        assert microsoftInfo != null; // NotNull!!!
        if (System.currentTimeMillis() - microsoftInfo.dateExpires > 0) {
            try {
                Pair<MicrosoftInfo, MinecraftResponse> authresponse = authMicrosoft(mca, microsoftInfo.getAccessToken(), microsoftInfo.getRefreshToken());
                microsoftInfo = authresponse.getKey();
                accessToken = authresponse.getValue().getAccessToken();
                return true;
            } catch (Exception e) {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * Serializes the UserProperties object for use in launching 1.7
     *
     * @param properties The raw properties JsonElement
     * @return A string representing the serialized json properties
     */
    public static String deserializeProperties(JsonElement properties) {
        HashMap<String, String> finalProperties = new HashMap<>();
        if (properties.isJsonArray()) {
            JsonArray arr = (JsonArray) properties;
            for (JsonElement element : arr) {
                if (!element.isJsonObject()) {
                    continue;
                }

                JsonObject obj = element.getAsJsonObject();
                if (obj.get("name") != null && obj.get("value") != null) {
                    finalProperties.put(obj.get("name").getAsString(), obj.get("value").getAsString());
                }
            }
        } else if (properties.isJsonObject()) {
            finalProperties = GSON.fromJson(properties, new TypeToken<HashMap<String, String>>() {}.getType());
        }
        return GSON.toJson(finalProperties);
    }

    /**
     * Fetch the path to this user's avatar.
     * @return The path
     * @throws IOException Thrown if fetching goes horribly wrong
     */
    public Path fetchAvatar() throws IOException {
        return fetchAvatar(uuid);
    }

    /**
     * Fetches a user's avatar using the given uuid.
     * @param uuid The user's uuid
     * @return A path to the user's avatar file
     * @throws IOException Thrown if getting the avatar goes horribly wrong
     */
    public static Path fetchAvatar(String uuid) throws IOException {
        Path location = Path.of("launcher/cache/avatars/" + uuid + ".png");
        if (!Files.exists(location, LinkOption.NOFOLLOW_LINKS)) {
            Files.createDirectories(location.getParent());

            Settings settings = ProtoLauncher.getInstance().getSettings();
            String endpoint = settings.getEndpoints().getAvatarApi().toString().replace("%uuid%", uuid);
            System.out.println("ENDPOINT: " + endpoint);
            Network.download(new URL(endpoint), location);
        }
        return location;
    }

    // Getters
    public String getUsername() {
        return username;
    }
    public String getUuid() {
        return uuid;
    }
    public String getUserProperties() {
        return userProperties;
    }
    public String getAccessToken() {
        return accessToken;
    }
    public @Nullable MicrosoftInfo getMicrosoftInfo() {
        return microsoftInfo;
    }

    // Stringify
    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", uuid='" + uuid + '\'' +
                ", userProperties='" + userProperties + '\'' +
                ", accessToken='" + accessToken + '\'' +
                ", microsoftInfo=" + microsoftInfo +
                '}';
    }

    // Sub-Objects
    public static class MicrosoftInfo {

        // Properties
        @Since(1.00)
        private String accessToken;

        @Since(1.00)
        private String refreshToken;

        @Since(1.00)
        private String xblToken;

        @Since(1.00)
        private String xblUhs;

        @Since(1.00)
        private String xstsToken;

        @Since(1.00)
        private long dateExpires;

        /**
         * Constructs a new MicrosoftInfo with all the nessecary data.
         *
         * @param accessToken The access token for the Microsoft account
         * @param refreshToken The refresh token for the Microsoft account
         * @param xblToken The Xbox Live access token
         * @param xblUhs The Xbox Live UHS
         * @param xstsToken The XSTS access token
         * @param dateExpires The date in which the tokens will expire
         */
        private MicrosoftInfo(String accessToken, String refreshToken, String xblToken, String xblUhs, String xstsToken, long dateExpires) {
            this.accessToken = accessToken;
            this.refreshToken = refreshToken;
            this.xblToken = xblToken;
            this.xblUhs = xblUhs;
            this.xstsToken = xstsToken;
            this.dateExpires = dateExpires;
        }

        // Getters
        public String getAccessToken() {
            return accessToken;
        }
        public String getRefreshToken() {
            return refreshToken;
        }
        public String getXblToken() {
            return xblToken;
        }
        public String getXblUhs() {
            return xblUhs;
        }
        public String getXstsToken() {
            return xstsToken;
        }
        public long getDateExpires() {
            return dateExpires;
        }

        // Stringify
        @Override
        public String toString() {
            return "MicrosoftInfo{" +
                    "accessToken='" + accessToken + '\'' +
                    ", refreshToken='" + refreshToken + '\'' +
                    ", xblToken='" + xblToken + '\'' +
                    ", xblUhs='" + xblUhs + '\'' +
                    ", xstsToken='" + xstsToken + '\'' +
                    ", dateExpires=" + dateExpires +
                    '}';
        }

    }

}
