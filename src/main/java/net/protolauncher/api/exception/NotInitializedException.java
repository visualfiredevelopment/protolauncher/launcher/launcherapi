package net.protolauncher.api.exception;

/**
 * Thrown if the launcher is not initialized.
 */
public class NotInitializedException extends Exception {

    public NotInitializedException() {
        super("The launcher has not been initialized!");
    }

}
