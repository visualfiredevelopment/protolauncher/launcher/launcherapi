package net.protolauncher.api.modding;

import org.jetbrains.annotations.Nullable;

/**
 * Contains information about a fabric version such as the download link, the Minecraft version it's for, and the date is was uploaded.
 * Can only be created via GSON parsing.
 */
public class FabricVersionInfo {

    // Instance Variables
    private String id;
    private String mcv;
    private FabricVersionType type;
    private String fileType;
    @Nullable private String filename;
    private String url;
    @Nullable private String time;

    // Constructor
    private FabricVersionInfo() { }

    // Getters
    public String getId() {
        return id;
    }
    public String getMcv() {
        return mcv;
    }
    public FabricVersionType getType() {
        return type;
    }
    public String getFileType() {
        return fileType;
    }
    public @Nullable String getFilename() {
        return filename;
    }
    public String getUrl() {
        return url;
    }
    public @Nullable String getTime() {
        return time;
    }

    // Stringify
    @Override
    public String toString() {
        return "FabricVersionInfo{" +
                "id='" + id + '\'' +
                ", mcv='" + mcv + '\'' +
                ", type=" + type +
                ", fileType='" + fileType + '\'' +
                ", filename='" + filename + '\'' +
                ", url='" + url + '\'' +
                ", time='" + time + '\'' +
                '}';
    }

}