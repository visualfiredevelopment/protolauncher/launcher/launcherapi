package net.protolauncher.api.modding;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javafx.util.Pair;
import net.protolauncher.mojang.version.VersionManifest;
import net.protolauncher.util.ISaveable;
import net.protolauncher.util.Network;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Arrays;

/**
 * Represents all Fabric versions publicly provided by ProtoLauncher's API.
 * Very similar to {@link VersionManifest}.
 * Can only be created via GSON parsing.
 */
public class FabricVersionManifest implements ISaveable {

    // Constants
    public static final Path FILE_LOCATION; // The file at which these settings are stored
    public static final Gson GSON; // A custom GSON
    private static FabricVersionManifest instance;

    // Static Initializer
    static {
        FILE_LOCATION = Path.of("mojang/fabric_manifest.json");
        GSON = new GsonBuilder().serializeNulls().create();
    }

    // Properties
    private FabricVersionInfo[] versions;

    // Constructor
    private FabricVersionManifest() { }

    /**
     * Fetches all versions of the corresponding Minecraft version from the manifest.
     *
     * @param mcv The Minecraft version string id
     * @return A {@link FabricVersionInfo} array containing the matched game version
     */
    public FabricVersionInfo[] getVersionsForMcv(String mcv) {
        return Arrays.stream(this.versions).filter(x -> x.getMcv().equalsIgnoreCase(mcv)).toArray(FabricVersionInfo[]::new);
    }

    /**
     * Fetches a specific version using the corresponding Minecraft version and id from the manifest.
     *
     * @param mcv The Minecraft version string id
     * @param id The Forge version string id
     * @return A {@link FabricVersionInfo} if found otherwise null
     */
    public @Nullable FabricVersionInfo getVersion(String mcv, String id) {
        return Arrays.stream(this.versions).filter(x -> x.getMcv().equalsIgnoreCase(mcv) && x.getId().equalsIgnoreCase(id)).findFirst().orElse(null);
    }

    /**
     * Loads a {@link FabricVersionManifest}.
     *
     * @param manifestUrl The {@link URL} at which to download the manifest if necessary
     * @param nextUpdate The next {@link Instant} that this manifest needs to be updated
     * @return A pair containing the {@link FabricVersionManifest} as its key and a boolean indicating whether or not it updated
     * @throws IOException Thrown if there was an exception while trying to load.
     */
    public static Pair<FabricVersionManifest, Boolean> load(URL manifestUrl, Instant nextUpdate) throws IOException {
        boolean updated;
        if (!Files.exists(FILE_LOCATION) || (Instant.now().isAfter(nextUpdate))) {
            // Download Manifest
            String string = Network.stringify(Network.fetch(manifestUrl));

            // Parse Manifest
            instance = GSON.fromJson(string, FabricVersionManifest.class);
            instance.save();
            updated = true;
        } else {
            if (instance == null) {
                instance = new FabricVersionManifest();
            }
            instance.load();
            updated = false;
        }
        return new Pair<>(instance, updated);
    }

    // Implement ISaveable
    @Override
    public Type getType() {
        return FabricVersionManifest.class;
    }
    @Override
    public Gson getGson() {
        return GSON;
    }
    @Override
    public Path getLocation() {
        return FILE_LOCATION;
    }

}
