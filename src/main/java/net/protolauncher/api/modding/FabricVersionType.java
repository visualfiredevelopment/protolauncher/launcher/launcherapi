package net.protolauncher.api.modding;

import com.google.gson.annotations.SerializedName;

public enum FabricVersionType {

    @SerializedName("stable")
    STABLE,

    @SerializedName("unstable")
    UNSTABLE

}