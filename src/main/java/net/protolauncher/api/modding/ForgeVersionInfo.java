package net.protolauncher.api.modding;

/**
 * Contains information about a forge version such as the download link, the Minecraft version it's for, and the date is was uploaded.
 * Can only be created via GSON parsing.
 */
public class ForgeVersionInfo {

    // Instance Variables
    private String id;
    private String mcv;
    private ForgeVersionType type;
    private String fileType;
    private String filename;
    private String url;
    private String time;

    // Constructor
    private ForgeVersionInfo() { }

    // Getters
    public String getId() {
        return id;
    }
    public String getMcv() {
        return mcv;
    }
    public ForgeVersionType getType() {
        return type;
    }
    public String getFileType() {
        return fileType;
    }
    public String getFilename() {
        return filename;
    }
    public String getUrl() {
        return url;
    }
    public String getTime() {
        return time;
    }

    // Stringify
    @Override
    public String toString() {
        return "ForgeVersionInfo{" +
                "id='" + id + '\'' +
                ", mcv='" + mcv + '\'' +
                ", type=" + type +
                ", fileType='" + fileType + '\'' +
                ", filename='" + filename + '\'' +
                ", url='" + url + '\'' +
                ", time='" + time + '\'' +
                '}';
    }

}