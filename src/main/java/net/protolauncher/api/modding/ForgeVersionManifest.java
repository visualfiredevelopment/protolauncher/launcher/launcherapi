package net.protolauncher.api.modding;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Pair;
import net.protolauncher.mojang.version.VersionManifest;
import net.protolauncher.util.ISaveable;
import net.protolauncher.util.Network;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Represents all Forge versions publicly provided by ProtoLauncher's API.
 * Very similar to {@link VersionManifest}.
 * Can only be created via GSON parsing.
 */
public class ForgeVersionManifest implements ISaveable {

    // Constants
    public static final Path FILE_LOCATION; // The file at which these settings are stored
    public static final Gson GSON; // A custom GSON
    private static ForgeVersionManifest instance;

    // Static Initializer
    static {
        FILE_LOCATION = Path.of("mojang/forge_manifest.json");
        GSON = new GsonBuilder().serializeNulls().create();
    }

    // Properties
    private ForgeVersionInfo[] versions;

    // Constructor
    private ForgeVersionManifest() { }

    /**
     * Fetches all versions of the corresponding Minecraft version from the manifest.
     *
     * @param mcv The Minecraft version string id
     * @return A {@link ForgeVersionInfo} array containing the matched game version
     */
    public ForgeVersionInfo[] getVersionsForMcv(String mcv) {
        return Arrays.stream(this.versions).filter(x -> x.getMcv().equalsIgnoreCase(mcv)).toArray(ForgeVersionInfo[]::new);
    }

    /**
     * Fetches a specific version using the corresponding Minecraft version and id from the manifest.
     *
     * @param mcv The Minecraft version string id
     * @param id The Forge version string id
     * @return A {@link ForgeVersionInfo} if found otherwise null
     */
    public @Nullable ForgeVersionInfo getVersion(String mcv, String id) {
        return Arrays.stream(this.versions).filter(x -> x.getMcv().equalsIgnoreCase(mcv) && x.getId().equalsIgnoreCase(id)).findFirst().orElse(null);
    }

    /**
     * Creates a new JavaFX window and prompts the user to skip the adfocus download window.
     *
     * @param owner The owner of the popup.
     * @param downloadUrl The URL for the adfocus download page.
     * @return A {@link CompletableFuture} that will timeout in 5 minutes containing either null if it error with no exception or a URL with the location of the direct download for the Forge installer
     */
    public static CompletableFuture<URL> promptForgeDownload(@Nullable Stage owner, String downloadUrl) {
        CompletableFuture<URL> future = new CompletableFuture<>();

        Platform.runLater(() -> {
            // Create popup window
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(owner);
            stage.setTitle("ProtoLauncher :: Forge Download");

            // Handle Stage Closure
            AtomicBoolean validClose = new AtomicBoolean(false);
            stage.setOnHiding(event -> {
                if (!validClose.get()) {
                    future.complete(null);
                    stage.close();
                }
            });

            // Create WebView w/ Cookies
            CookieManager manager = new CookieManager();
            CookieHandler.setDefault(manager);
            WebView view = new WebView();
            WebEngine engine = view.getEngine();
            engine.locationProperty().addListener(event -> {
                String location = view.getEngine().getLocation();

                try {
                    URL url = new URL(location);
                    if (url.getPath().endsWith("installer.jar")) {
                        future.complete(url);
                        stage.close();
                    }
                } catch (Exception e) {
                    future.completeExceptionally(e);
                    stage.close();
                }
            });

            // Create and apply scene
            Scene scene = new Scene(view, 1350, 900);
            stage.setScene(scene);
            stage.show();

            // Load the URL
            engine.load(downloadUrl);

            // Handle Timeout
            future.exceptionally(e -> {
                if (e instanceof TimeoutException) {
                    validClose.set(true);
                    stage.close();
                }
                return null;
            });
            future.orTimeout(5, TimeUnit.MINUTES);
        });

        // Return the future
        return future;
    }

    /**
     * Loads a {@link ForgeVersionManifest}.
     *
     * @param manifestUrl The {@link URL} at which to download the manifest if necessary
     * @param nextUpdate The next {@link Instant} that this manifest needs to be updated
     * @return A pair containing the {@link ForgeVersionManifest} as its key and a boolean indicating whether or not it updated
     * @throws IOException Thrown if there was an exception while trying to load.
     */
    public static Pair<ForgeVersionManifest, Boolean> load(URL manifestUrl, Instant nextUpdate) throws IOException {
        boolean updated;
        if (!Files.exists(FILE_LOCATION) || (Instant.now().isAfter(nextUpdate))) {
            // Download Manifest
            String string = Network.stringify(Network.fetch(manifestUrl));

            // Parse Manifest
            instance = GSON.fromJson(string, ForgeVersionManifest.class);
            instance.save();
            updated = true;
        } else {
            if (instance == null) {
                instance = new ForgeVersionManifest();
            }
            instance.load();
            updated = false;
        }
        return new Pair<>(instance, updated);
    }

    // Implement ISaveable
    @Override
    public Type getType() {
        return ForgeVersionManifest.class;
    }
    @Override
    public Gson getGson() {
        return GSON;
    }
    @Override
    public Path getLocation() {
        return FILE_LOCATION;
    }

}
