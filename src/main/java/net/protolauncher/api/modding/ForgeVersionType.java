package net.protolauncher.api.modding;

import com.google.gson.annotations.SerializedName;

public enum ForgeVersionType {

    @SerializedName("latest")
    LATEST,

    @SerializedName("recommended")
    RECOMMENDED

}