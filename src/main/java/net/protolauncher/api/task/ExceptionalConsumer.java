package net.protolauncher.api.task;

/**
 * An implementation of the consumer interface that allows for exceptions.
 * Useful when in combination with tasks.
 */
@FunctionalInterface
public interface ExceptionalConsumer<T>  {
    void accept(T t) throws Exception;
}