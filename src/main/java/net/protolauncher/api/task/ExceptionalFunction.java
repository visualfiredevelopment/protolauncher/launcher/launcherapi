package net.protolauncher.api.task;

/**
 * An implementation of the function interface that allows for exceptions.
 * Useful when in combination with tasks.
 */
@FunctionalInterface
public interface ExceptionalFunction<T, R>  {
    R apply(T t) throws Exception;
}