package net.protolauncher.api.task;

/**
 * An implementation of the runnable interface that allows for exceptions.
 * Useful when in combination with tasks.
 */
@FunctionalInterface
public interface ExceptionalRunnable  {
    void run() throws Exception;
}