package net.protolauncher.api.task;

/**
 * This class allows for a task to wrap a void-returning method, giving the following implementation:
 * <pre>{@code
 *     public void initialize(TaskFeedback<Void> feedback) { }
 *     ...
 *     LauncherMethodTask<Void> task = new LauncherMethodTask<>(launcher::initialize);
 * }</pre>
 */
public class LauncherConsumableTask extends LauncherTask<Void> {

    // Variables
    private final ExceptionalConsumer<TaskFeedback<Void>> consumer;

    /**
     * Constructs a new task that takes a consumer as input.
     * @param consumer The consumer for this task.
     */
    public LauncherConsumableTask(ExceptionalConsumer<TaskFeedback<Void>> consumer) {
        this.consumer = consumer;
    }

    @Override
    protected Void call() throws Exception {
        consumer.accept(new TaskFeedback<>(this));
        return null;
    }

}
