package net.protolauncher.api.task;

/**
 * This class allows for a task to wrap an object-returning method, giving the following implementation:
 * <pre>{@code
 *     public boolean initialize(TaskFeedback<Boolean> feedback) { }
 *     ...
 *     LauncherMethodTask<Boolean> task = new LauncherMethodTask<>(launcher::initialize);
 * }</pre>
 */
public class LauncherFunctionalTask<V> extends LauncherTask<V> {

    // Variables
    private final ExceptionalFunction<TaskFeedback<V>, V> function;

    /**
     * Constructs a new task that takes a function as input.
     * @param function The function for this task.
     */
    public LauncherFunctionalTask(ExceptionalFunction<TaskFeedback<V>, V> function) {
        this.function = function;
    }

    @Override
    protected V call() throws Exception {
        return function.apply(new TaskFeedback<>(this));
    }

}
