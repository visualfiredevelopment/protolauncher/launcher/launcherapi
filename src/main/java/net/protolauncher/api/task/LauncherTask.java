package net.protolauncher.api.task;

import javafx.application.Platform;
import javafx.concurrent.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayDeque;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.function.Consumer;

/**
 * An extension of the JavaFX {@link Task<V>} which adds better event handling for more useful UI updates.
 * <br/>
 * <br/>
 * Inspired by <a href="https://stackoverflow.com/questions/40196020/">this stackoverflow question</a>.
 */
public abstract class LauncherTask<V> extends Task<V> {

    // Event Consumers
    private Consumer<String> messageHandler;
    private Consumer<Progress> progressHandler;
    private Consumer<String> titleHandler;
    private Consumer<V> valueHandler;

    // Message Queue
    private final Queue<String> messageQueue = new ArrayDeque<>();
    private boolean messageUpdating;
    private final Runnable messageUpdater = () -> {
        synchronized (messageQueue) {
            String lastMessage = null;
            String message;
            do {
                message = messageQueue.poll();
                if (message != null) {
                    lastMessage = message;
                    if (messageHandler != null) {
                        messageHandler.accept(message);
                    }
                }
            } while (message != null);
            if (lastMessage != null) {
                super.updateMessage(lastMessage); // Maintain previous (imho, useless) functionality
            }
            messageQueue.clear();
            messageUpdating = false;
        }
    };

    // Progress Queue
    private final Queue<Progress> progressQueue = new ArrayDeque<>();
    private boolean progressUpdating;
    private final Runnable progressUpdater = () -> {
        synchronized (progressQueue) {
            Progress lastProgress = null;
            Progress progress;
            do {
                progress = progressQueue.poll();
                if (progress != null) {
                    lastProgress = progress;
                    if (progressHandler != null) {
                        progressHandler.accept(progress);
                    }
                }
            } while (progress != null);
            if (lastProgress != null) {
                super.updateProgress(lastProgress.getWorkDone(), lastProgress.getMax());
            }
            progressQueue.clear();
            progressUpdating = false;
        }
    };

    // Title Queue
    private final Queue<String> titleQueue = new ArrayDeque<>();
    private boolean titleUpdating;
    private final Runnable titleUpdater = () -> {
        synchronized (titleQueue) {
            String lastTitle = null;
            String title;
            do {
                title = titleQueue.poll();
                if (title != null) {
                    lastTitle = title;
                    if (titleHandler != null) {
                        titleHandler.accept(title);
                    }
                }
            } while (title != null);
            if (lastTitle != null) {
                super.updateTitle(lastTitle);
            }
            titleQueue.clear();
            titleUpdating = false;
        }
    };

    // Value Queue
    private final Queue<Optional<V>> valueQueue = new ArrayDeque<>();
    private boolean valueUpdating;
    private final Runnable valueUpdater = () -> {
        synchronized (valueQueue) {
            Optional<V> value;
            do {
                value = valueQueue.poll();
                if (valueHandler != null && Objects.nonNull(value)) {
                    valueHandler.accept(value.orElse(null));
                }
            } while (Objects.nonNull(valueQueue.peek()));
            assert Objects.nonNull(value);
            super.updateValue(value.orElse(null));
            valueQueue.clear();
            valueUpdating = false;
      }
    };

    // Override UpdateMessage Function
    @Override
    protected void updateMessage(@NotNull String message) {
        synchronized (messageQueue) {
            messageQueue.add(message);
            if (!messageUpdating) {
                messageUpdating = true;
                Platform.runLater(messageUpdater);
            }
        }
    }

    // Override UpdateProgress Function
    @Override
    protected void updateProgress(double workDone, double max) {
        synchronized (progressQueue) {
            progressQueue.add(new Progress(workDone, max));
            if (!progressUpdating) {
                progressUpdating = true;
                Platform.runLater(progressUpdater);
            }
        }
    }

    // Override UpdateTitle Function
    @Override
    protected void updateTitle(@NotNull String title) {
        synchronized (titleQueue) {
            titleQueue.add(title);
            if (!titleUpdating) {
                titleUpdating = true;
                Platform.runLater(titleUpdater);
            }
        }
    }

    // Override UpdateValue Function
    @Override
    protected void updateValue(@Nullable V value) {
        synchronized (valueQueue) {
            valueQueue.add(Optional.ofNullable(value));
            if (!valueUpdating) {
                valueUpdating = true;
                Platform.runLater(valueUpdater);
            }
        }
    }

    // Setters
    public LauncherTask<V> setMessageHandler(Consumer<String> messageHandler) {
        this.messageHandler = messageHandler;
        return this;
    }
    public LauncherTask<V> setProgressHandler(Consumer<Progress> progressHandler) {
        this.progressHandler = progressHandler;
        return this;
    }
    public LauncherTask<V> setTitleHandler(Consumer<String> titleHandler) {
        this.titleHandler = titleHandler;
        return this;
    }
    public LauncherTask<V> setValueHandler(Consumer<V> valueHandler) {
        this.valueHandler = valueHandler;
        return this;
    }

    // Progress Class
    public static class Progress {

        // Variables
        private Double workDone;
        private Double max;

        // Constructor
        public Progress(@NotNull Double workDone, Double max) {
            this.workDone = workDone;
            this.max = max;
        }

        // Getters
        @NotNull
        public Double getWorkDone() {
            return workDone;
        }
        public Double getMax() {
            return max;
        }

        // Setters
        public void setWorkDone(Double workDone) {
            this.workDone = workDone;
        }
        public void setMax(Double max) {
            this.max = max;
        }

    }

    // Anonymous Feedback Class
    public static class TaskAnonymousFeedback {

        // Variables
        protected final LauncherTask<?> task;

        /**
         * Constructs a new anonymous "task feedback" that can be used to temporarily
         * expose the update methods, except for updateValue.
         *
         * @param task The task to expose the methods for.
         */
        public TaskAnonymousFeedback(LauncherTask<?> task) {
            this.task = task;
        }

        // Expose Update Methods
        public void updateMessage(String message) {
            task.updateMessage(message);
        }
        public void updateProgress(double workDone, double max) {
            task.updateProgress(workDone, max);
        }
        public void updateProgress(long workDone, long max) {
            task.updateProgress(workDone, max);
        }
        public void updateTitle(String title) {
            task.updateTitle(title);
        }
        public boolean isCancelled() {
            return task.isCancelled();
        }

    }

    // Feedback Class
    public static class TaskFeedback<V> extends TaskAnonymousFeedback {

        /**
         * Constructs a new "task feedback" that can be used to temporarily
         * expose the update methods.
         *
         * @param task The task to expose the methods for.
         */
        public TaskFeedback(LauncherTask<V> task) {
            super(task);
        }

        // Expose Update Methods
        public void updateValue(V value) {
            ((LauncherTask<V>) task).updateValue(value);
        }

    }

    // Dummy Feedback Class
    public static class DummyTaskFeedback<V> extends TaskFeedback<V> {

        /**
         * Constructs a dummy task feedback to allow for methods to be called without actually using a task.
         */
        public DummyTaskFeedback() {
            super(null);
        }

        // Override Old Methods
        public void updateMessage(String message) {

        }
        public void updateProgress(double workDone, double max) {

        }
        public void updateProgress(long workDone, long max) {

        }
        public void updateTitle(String title) {

        }
        public void updateValue(V value) {

        }
        public boolean isCancelled() {
            return false;
        }

    }

}
