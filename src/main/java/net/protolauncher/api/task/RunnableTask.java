package net.protolauncher.api.task;

import javafx.concurrent.Task;
import net.protolauncher.api.task.LauncherTask.TaskFeedback;

/**
 * This class allows for a task to wrap a void-returning method with no arguments, giving the following implementation:
 * <pre>{@code
 *     public void initialize() { }
 *     ...
 *     LauncherRunnableTask<Void> task = new LauncherRunnableTask<>(launcher::initialize);
 * }</pre>
 * <b>WARNING</b>: This class does NOT support {@link TaskFeedback}. It is a REGULAR task, not a LauncherTask.
 */
public class RunnableTask extends Task<Void> {

    // Variables
    private final ExceptionalRunnable runnable;

    /**
     * Constructs a new task that takes a runnable as input.
     * @param runnable The runnable for this task.
     */
    public RunnableTask(ExceptionalRunnable runnable) {
        this.runnable = runnable;
    }

    @Override
    protected Void call() throws Exception {
        runnable.run();
        return null;
    }

}